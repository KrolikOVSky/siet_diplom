package ru.krolikovsky.siet.webapp.controller;


import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.krolikovsky.siet.common.dto.PageDto;
import ru.krolikovsky.siet.common.web.MarmelabUtils;
import ru.krolikovsky.siet.webapp.model.dto.UserDto;
import ru.krolikovsky.siet.webapp.model.dto.UserEditRequest;
import ru.krolikovsky.siet.webapp.model.dto.UserWithPasswordDto;
import ru.krolikovsky.siet.webapp.service.UserService;

import javax.validation.Valid;
import java.util.List;

import static ru.krolikovsky.siet.common.Constants.API_PATH_PREFIX_WEBAPP;

/**
 * Класс-контроллер реализующий добавление, редактирование, удаление пользователей, а также
 * получение списка всех пользователей либо каждого отдельно. Используется путь "/siet-api/users"
 */

@RestController
@RequestMapping(path = API_PATH_PREFIX_WEBAPP + "/users")
@AllArgsConstructor
public class UserController {
    private UserService userService;

    @GetMapping
    public ResponseEntity<List<UserDto>> listUsersByName(@RequestParam(required = false) Integer _end,
                                                         @RequestParam(required = false) Integer _start,
                                                         @RequestParam(required = false) String _sort,
                                                         @RequestParam(required = false) String _order,
                                                         @RequestParam(required = false) String name
    ) {
        PageDto<UserDto> users = userService.listUsersByName(MarmelabUtils.makePageRequest(_start, _end, _sort, _order), name);

        return ResponseEntity.ok()
                .header(MarmelabUtils.TABLE_TOTAL_COUNT_HEADER, Long.toString(users.getTotal()))
                .body(users.getDtos());
    }

    @GetMapping("list")
    public ResponseEntity<List<UserDto>> listUsersByName(@RequestParam(required = false) String name) {
        return ResponseEntity.ok()
                .body(userService.listUsersByName(name));
    }

    /**
     * Метод getUser служит для получения информации об одном пользователе
     *
     * @param userId id пользователя, который был передан в пути запроса
     * @return возвращает тело ответа содержащее http заголовки и данные самого пользователя
     */
    @GetMapping("/{userId}")
    public ResponseEntity<UserDto> getUser(@PathVariable(value = "userId") String userId) {
        return ResponseEntity.ok()
                .body(userService.getUser(userId));
    }

    /**
     * Метод {@code editUser} служит для изменения информации о пользователе
     *
     * @param id                  идентификационный номер пользователя в системе
     * @param userEditRequest тело запроса содержащее все поля для редактирования
     * @return Метод возвращает пользователя с изменёнными данными
     */
    @PutMapping("/{id}")
    public ResponseEntity<UserDto> editUser(@PathVariable String id, @RequestBody @Valid UserEditRequest userEditRequest) {
        return ResponseEntity.ok()
                .body(userService.update(id, userEditRequest));
    }


    /**
     * Метод {@code createUser} реализует добавление нового пользователя в систему
     *
     * @param userWithPasswordDto тело запроса с необходимыми полями для добавления пользователя
     * @return Метод возвращает данные о новом пользователе в системе
     */

    @PostMapping
    public ResponseEntity<UserDto> createUser(@RequestBody @Valid UserWithPasswordDto userWithPasswordDto) {
        return ResponseEntity.ok()
                .body(userService.create(userWithPasswordDto));
    }

    /**
     * Метод {@code deleteUser} реализует удаление пользователя по его ID
     *
     * @param id идентификационный номер пользователя, которого необходимо удалить
     * @return Метод возвращает информацию об удалённом пользователе
     */

    @DeleteMapping("/{id}")
    public ResponseEntity<UserDto> deleteUser(@PathVariable String id) {
        return ResponseEntity.ok()
                .body(userService.delete(id));
    }
}
