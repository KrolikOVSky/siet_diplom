package ru.krolikovsky.siet.webapp.model.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Класс {@code UserEditRequest} для передачи данных для редактирования информации о пользователе
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class UserEditRequest extends UserWithPasswordDto {
    private String currentPassword;
}