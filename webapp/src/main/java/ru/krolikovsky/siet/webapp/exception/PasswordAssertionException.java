package ru.krolikovsky.siet.webapp.exception;

import ru.krolikovsky.siet.common.SietException;

/**
 * Класс-исключение предназначенный для оповещения о том что введенный пользователем пароль неправильный
 */

public class PasswordAssertionException extends SietException {

    /**
     * Конструктор {@code PasswordAssertionException} для реализации исключения со следующим сообщением
     * <ul>
     *  <li>
     *      <i>
     *          "Введен неправильный пароль"
     *      </i>
     *  </li>
     * </ul>
     */

    public PasswordAssertionException() {
        super("Введен неправильный пароль");
    }
}
