package ru.krolikovsky.siet.webapp.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import ru.krolikovsky.siet.common.model.Role;


@Data
@Builder
public class UserLoggingDetailsDto {
    private String id;
    private String name;
    private String login;
    @JsonProperty("isDisabled")
    private boolean isDisabled;
    private Role role;
}