package ru.krolikovsky.siet.webapp.model.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Класс {@code UserWithPasswordDto} для передачи данных о пользователе с паролем
 */


@EqualsAndHashCode(callSuper = true)
@Data
public class UserWithPasswordDto extends UserDto{
    private String password;
}