package ru.krolikovsky.siet.webapp.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.CorsFilter;
import ru.krolikovsky.siet.common.auth.JwtTokenFilter;
import ru.krolikovsky.siet.common.auth.SecurityConfigUtils;

import javax.annotation.Resource;

import static ru.krolikovsky.siet.common.Constants.API_PATH_PREFIX;
import static ru.krolikovsky.siet.common.Constants.API_PATH_PREFIX_WEBAPP;

/**
 * {@code SecurityConfig} - это класс-конфигурация для фильтрации http запросов
 *
 * @see <a href="https://www.toptal.com/spring/spring-security-tutorial">Spring Security Tutorial</a>
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)
@Slf4j
public class SecurityConfig {


    @Resource
    private SecurityConfigUtils securityConfigUtils;
    @Resource
    private PasswordEncoder passwordEncoder;
    @Resource
    private JwtTokenFilter jwtTokenFilter;
    @Resource(name = "localUserService")
    private UserDetailsService localUserDetailsService;

    @Configuration
    public class JwtAuthenticationConfig {

        @Bean
        public SecurityFilterChain jwtFilterChain(HttpSecurity http) throws Exception {
            // Enable CORS and disable CSRF
            http = securityConfigUtils.configureSharedSetting(http);

            // Set permissions on endpoints
            http.authorizeRequests()
                    // Our public endpoints
                    .antMatchers("/" + API_PATH_PREFIX + "/public/**").permitAll()
                    .antMatchers("/" + API_PATH_PREFIX_WEBAPP + "/auth/**").permitAll()
                    // Our private endpoints
                    .anyRequest().authenticated();

            // Add JWT token filter
            http.addFilterBefore(
                    jwtTokenFilter,
                    UsernamePasswordAuthenticationFilter.class
            );
            return http.build();
        }

    }

    @Bean
    @Primary
    public AuthenticationManager authenticationManager(HttpSecurity http) throws Exception {
        AuthenticationManagerBuilder builder = http.getSharedObject(AuthenticationManagerBuilder.class)
                .authenticationProvider(localAuthProvider());
        return builder.build();
    }


    @Bean
    public DaoAuthenticationProvider localAuthProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(localUserDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder);
        return authProvider;
    }


    /**
     * Метод {@code corsFilter} для фильтрации общего доступа к ресурсам для разных источников
     *
     * @return возвращает экземпляр класса {@link CorsFilter}
     */
    @Bean
    public CorsFilter corsFilter() {
        return securityConfigUtils.corsFilter();
    }
}
