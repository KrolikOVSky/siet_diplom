package ru.krolikovsky.siet.webapp.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import ru.krolikovsky.siet.common.model.Role;

import javax.validation.constraints.Size;


/**
 * Класс {@code UserDto} для шаблона разрешенных входящих и исходящих данных пользователя
 */

@Data
public class UserDto {
    private String id;
    @Size(max = 256, message = "Имя должно быть не длиннее {max} символов")
    private String name;
    @Size(max = 20, message = "Логин должен быть не длиннее {max} символов")
    private String login;
    @JsonProperty("isDisabled")
    private boolean isDisabled;
    private Role role;
    private int failedLoginAttemptsCount = 0;
    private String email;
}
