package ru.krolikovsky.siet.webapp.controller.auth;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.krolikovsky.siet.common.auth.JwtTokenUtil;
import ru.krolikovsky.siet.webapp.model.User;
import ru.krolikovsky.siet.webapp.model.dto.AuthRequest;
import ru.krolikovsky.siet.webapp.model.dto.UserDto;
import ru.krolikovsky.siet.webapp.model.dto.UserWithPasswordDto;
import ru.krolikovsky.siet.webapp.service.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import static ru.krolikovsky.siet.common.Constants.API_PATH_PREFIX_WEBAPP;

/**
 * Класс {@code AuthController} реализует запрос аутентификацию клиента по пути '/api/auth'
 */

@RestController
@RequestMapping(path = API_PATH_PREFIX_WEBAPP + "/auth")
public class AuthController {

    @Resource
    private UserService userService;
    @Resource
    private JwtTokenUtil jwtTokenUtil;
    @Resource(name = "sietAuthenticationManager")
    private AuthenticationManager authenticationManager;

    public enum AuthResult {
        OK,
    }

    /**
     * Метод {@code login} реализует POST запрос на аутентификацию клиента
     *
     * @param request  Тело запроса, содержащее информацию о пользователе который проходит аутентификацию (логин\пароль).
     * @return возвращает тело ответа содержащее http заголовки с нужным ответом и данные об аутентифицированном пользователи:
     * <ul>
     *      <li> Возможные типы ответа в заголовке
     *          <ul>
     *              <li>  401 UNAUTHORIZED - если не прошел аутентификацию </li>
     *              <li>  200 OK - если пользователь был найден в системе и аутентифицирован,
     *                             в заголовке ответа возвращается JWT token </li>
     *          </ul>
     *      </li>
     * </ul>
     * @see ResponseEntity
     * @see AuthRequest
     * @see HttpServletResponse
     */

    @PostMapping("login")
    public ResponseEntity<AuthResult> login(@RequestBody @Valid AuthRequest request) {
        Authentication authenticate = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(request.getLogin(), request.getPassword()));
        UserDetails authenticatedUser = (UserDetails) authenticate.getPrincipal();
        User user = userService.loginUser(authenticatedUser.getUsername());
        String jwtToken = jwtTokenUtil.generateAccessToken(user);

        return ResponseEntity.ok()
                .header(HttpHeaders.AUTHORIZATION, jwtToken)
                .body(AuthResult.OK);
    }

    @PostMapping("sign-up")
    public ResponseEntity<UserDto> signUp(@RequestBody UserWithPasswordDto userWithPasswordDto) {
        return ResponseEntity.ok()
                .body(userService.signUp(userWithPasswordDto));
    }
}
