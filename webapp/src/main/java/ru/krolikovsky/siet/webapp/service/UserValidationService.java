package ru.krolikovsky.siet.webapp.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.krolikovsky.siet.webapp.exception.LoginAlreadyExistsException;
import ru.krolikovsky.siet.webapp.model.User;
import ru.krolikovsky.siet.webapp.repository.UserRepository;

import javax.annotation.Resource;
import java.util.Optional;

@Transactional(readOnly = true)
@Service
public class UserValidationService {
    @Resource
    private UserRepository userRepository;

    /**
     * Метод {@code checkUserLogin} выполняет проверку уникальности пользователя по его логину
     *
     * @param user пользователь, которого необходимо проверить
     */

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void checkUserLogin(User user) {
        Optional.ofNullable(userRepository.findByLogin(user.getLogin()))
                .ifPresent(existingUser -> {
                    if (user.getId() == null || !user.getId().equals(existingUser.getId())) {
                        throw new LoginAlreadyExistsException(user.getLogin());
                    }
                });
    }
}