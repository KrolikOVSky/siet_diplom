package ru.krolikovsky.siet.webapp.auth;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import ru.krolikovsky.siet.common.CredentialsException;
import ru.krolikovsky.siet.webapp.service.UserService;

import javax.annotation.Resource;

@Component("sietAuthenticationManager")
public class AuthenticationManager implements org.springframework.security.authentication.AuthenticationManager {
    @Resource
    private org.springframework.security.authentication.AuthenticationManager authenticationManager;
    @Resource
    private UserService userService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            return authenticationManager.authenticate(authentication);
        } catch (BadCredentialsException exception) {
            int attemptsCounts = userService.increaseFailedLoginAttemptsCount((String) authentication.getPrincipal());
            throw new CredentialsException(attemptsCounts);
        }
    }
}
