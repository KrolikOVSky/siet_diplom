package ru.krolikovsky.siet.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.krolikovsky.siet.api.ApiApplication;
import ru.krolikovsky.siet.common.CommonApplication;

@SpringBootApplication(scanBasePackageClasses = {CommonApplication.class,
        WebappApplication.class, ApiApplication.class})
public class WebappApplication {
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    public static void main(String[] args) {
        SpringApplication.run(WebappApplication.class, args);
    }

}
