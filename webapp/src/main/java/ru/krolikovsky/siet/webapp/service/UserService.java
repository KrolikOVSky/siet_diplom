package ru.krolikovsky.siet.webapp.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.krolikovsky.siet.common.CredentialsException;
import ru.krolikovsky.siet.common.auth.LoggedUserHolder;
import ru.krolikovsky.siet.common.authorization.ForAdmin;
import ru.krolikovsky.siet.common.authorization.ForStudent;
import ru.krolikovsky.siet.common.authorization.ForTeacher;
import ru.krolikovsky.siet.common.dto.PageDto;
import ru.krolikovsky.siet.common.model.Role;
import ru.krolikovsky.siet.webapp.auth.AuthenticatedUserDetails;
import ru.krolikovsky.siet.webapp.exception.PasswordAssertionException;
import ru.krolikovsky.siet.webapp.model.User;
import ru.krolikovsky.siet.webapp.model.dto.AuthRequest;
import ru.krolikovsky.siet.webapp.model.dto.UserDto;
import ru.krolikovsky.siet.webapp.model.dto.UserEditRequest;
import ru.krolikovsky.siet.webapp.model.dto.UserWithPasswordDto;
import ru.krolikovsky.siet.webapp.model.mappers.UserMapper;
import ru.krolikovsky.siet.webapp.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static ru.krolikovsky.siet.common.util.UuidConverter.toUuid;

/**
 * Сервисы для манипуляции данными пользователя
 */

@Primary
@Service("localUserService")
@RequiredArgsConstructor
public class UserService implements UserDetailsService {
    @NonNull
    private UserRepository userRepository;
    @NonNull
    private PasswordEncoder passwordEncoder;
    @NonNull
    private UserValidationService userValidationService;
    @NonNull
    private UserMapper userMapper;
    @NonNull
    private LoggedUserHolder loggedUserHolder;

    /**
     * Метод {@code listUsers} реализует извлечение пользователей из базы данных по заданному запросу
     *
     * @param pageRequest параметр запроса на извлечение
     * @return возвращает {@link PageDto объект} содержащий список пользователей и их количество
     * @see PageDto
     * @see PageRequest
     * @see Page
     */
    public PageDto<UserDto> listUsersByName(PageRequest pageRequest, String name) {
        Page<User> users;
        if (StringUtils.hasText(name)) {
            users = userRepository.findByNameContainingIgnoreCase(name, pageRequest);
        } else {
            users = userRepository.findAll(pageRequest);
        }
        return new PageDto<>(users.stream()
                .map(user -> userMapper.toUserDto(user))
                .toList(),
                users.getTotalElements()
        );
    }

    /**
     * Возращает всех пользователей чье имя содержит @name. Если параметр @name пустой, возвращает
     * всех существующих пользователей
     */
    public List<UserDto> listUsersByName(String name) {
        List<User> users;
        if (StringUtils.hasText(name)) {
            users = userRepository.findByNameContainingIgnoreCase(name);
        } else {
            users = userRepository.findAll();
        }
        return userMapper.toUserDtos(users);
    }

    /**
     * Метод {@code save} реализует сохранение пользователя в базу данных
     *
     * @param user              данные пользователя, которого необходимо добавить
     * @param isPasswordChanged произошла ли замена пароля
     */

    @Transactional
    //@PreAuthorize("principal.username.equals(#user.getLogin()) OR hasAuthority('ADMIN')")
    public void save(User user, boolean isPasswordChanged) {
        if (isPasswordChanged) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        userValidationService.checkUserLogin(user);
        userRepository.saveAndFlush(user);
    }

    /**
     * Метод {@code loginUser} авторизует необходимого пользователя
     *
     * @param login логин пользователя
     * @return возвращает данные о пользователе
     */

    public User loginUser(String login) {
        User user = userRepository.findByLogin(login);
        user.setFailedLoginAttemptsCount(0);
        userRepository.saveAndFlush(user);

        return user;
    }

    /**
     * Метод {@code loadUserById} извлекает пользователя из базы данных по его ID
     *
     * @param id идентификационный номер пользователя, данные которого необходимо получить
     * @return экземпляр класса {@link User}, найденный по ID
     */

    public User loadUserById(String id) {
        return userRepository.findById(UUID.fromString(id)).orElseThrow();
    }

    /**
     * Метод {@code isUserActive} реализует проверку наличия пользователя в базе
     *
     * @param id идентификационный номер пользователя
     * @return <ul>
     * <li> true - если пользователь есть в базе </li>
     * <li> false - если пользователь в базе отсутствует </li>
     * </ul>
     */

    public boolean isUserActive(String id) {
        return userRepository.findById(UUID.fromString(id)).isPresent();
    }

    /**
     * Метод {@code checkUserPassword} проверяет корректность пароля пользователя
     *
     * @param userId   идентификационный номер пользователя
     * @param password пароль, который необходимо проверить
     */

    public void checkUserPassword(String userId, String password) {
        if (!passwordEncoder.matches(password, loadUserById(userId).getPassword())) {
            throw new PasswordAssertionException();
        }
    }

    /**
     * Метод {@code loadUserByUsername} извлекает пользователя из базы данных по его логину
     *
     * @param username логин пользователя, которого необходимо найти
     * @return возвращает полные данные пользователя
     * @throws UsernameNotFoundException Исключение если пользователь не найден
     */

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByLogin(username);

        if (null == user) {
            throw new UsernameNotFoundException(null);
        }

        return AuthenticatedUserDetails.builder()
                .id(user.getIdAsString())
                .password(user.getPassword())
                .username(user.getLogin())
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .authorities(Collections.singleton(new SimpleGrantedAuthority(user.getRole().name())))
                .enabled(!user.isDisabled())
                .build();

    }


    @Transactional
    public UserDto getUser(String userId){
        User user = loadUserById(userId);
        return userMapper.toUserDto(user);
    }

    @Transactional
    public UserDto create(UserWithPasswordDto userDtoSectionsIdWithPassword) {
        User user = userMapper.fromCreateUserDto(userDtoSectionsIdWithPassword);
        save(user, true);
        return userMapper.toUserDto(user);
    }

    @Transactional
    @ForAdmin
    @ForStudent
    @ForTeacher
    public UserDto update(String id, UserEditRequest userEditRequest) {
        String currentUserId = loggedUserHolder.getAuthenticatedUserDetails().getId();
        if (currentUserId.equals(id)) {
            checkUserPassword(currentUserId, userEditRequest.getCurrentPassword());
        }
        User existingUser = loadUserById(id);
        userMapper.copyForUpdate(userEditRequest, existingUser);
        Optional.ofNullable(userEditRequest.getPassword())
                .ifPresent(existingUser::setPassword);
        if(!existingUser.isDisabled()) {
            existingUser.setFailedLoginAttemptsCount(0);
        }
        save(existingUser, userEditRequest.getPassword() != null);
        return userMapper.toUserDto(existingUser);
    }

    /**
     * Метод {@code isUserHasRole} реализует проверку наличия переданной роли у переданного пользователя
     *
     * @param userId идентификационный номер пользователя
     * @param role   необходимая для проверки роль
     * @return <ul>
     *              <li> true - если роль у данного пользователя есть
     *              <li> false - если пользователь не найден, либо у него другая роль
     *         </ul>
     */
    public boolean isUserHasRole(String userId, Role role) {
        return Optional.ofNullable(toUuid(userId))
                .flatMap(userRepository::findById)
                .map(User::getRole)
                .map(Role::name)
                .map(role::equals)
                .orElse(false);
    }


    /**
     * Метод {@code delete} реализует удаление пользователя из базы данных
     *
     * @param id ID пользователя, которого необходимо удалить
     */

    @Transactional
    @ForAdmin
    public UserDto delete(String id) {
        User user = loadUserById(id);
        user.setDeleted(true);
        userRepository.saveAndFlush(user);

        return userMapper.toUserDto(user);
    }


    public int increaseFailedLoginAttemptsCount(String login) {
        User user = userRepository.findByLogin(login);
        if (user == null) {
            throw new CredentialsException();
        }
        user.setFailedLoginAttemptsCount(user.getFailedLoginAttemptsCount() + 1);
        if (user.getFailedLoginAttemptsCount() >= 3) {
            user.setDisabled(true);
        }
        userRepository.saveAndFlush(user);
        return user.getFailedLoginAttemptsCount();
    }

    public UserDto signUp(UserWithPasswordDto userWithPasswordDto) {
        return this.create(userWithPasswordDto);
    }
}
