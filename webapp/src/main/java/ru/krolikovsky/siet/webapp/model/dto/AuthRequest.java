package ru.krolikovsky.siet.webapp.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Класс {@code AuthRequest} для шаблона входящих и исходящих данных запроса на аутентификацию
 */
@Data
public class AuthRequest {
    @NotNull
    private String login;
    @NotNull
    private String password;
}
