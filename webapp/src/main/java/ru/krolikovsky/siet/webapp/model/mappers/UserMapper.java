package ru.krolikovsky.siet.webapp.model.mappers;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.springframework.stereotype.Component;
import ru.krolikovsky.siet.common.mapper.BaseMapper;
import ru.krolikovsky.siet.common.mapper.NullableMapper;
import ru.krolikovsky.siet.webapp.model.User;
import ru.krolikovsky.siet.webapp.model.dto.UserDto;
import ru.krolikovsky.siet.webapp.model.dto.UserWithPasswordDto;

import java.util.List;

import static org.mapstruct.NullValueCheckStrategy.ALWAYS;
import static org.mapstruct.NullValuePropertyMappingStrategy.IGNORE;

/**
 * Класс для генерирования объектов переноса данных (DTO)
 */

@Component
@Mapper(componentModel = "spring", uses = NullableMapper.class)
public abstract class UserMapper extends BaseMapper {

    /**
     * Метод преобразует объект для переноса данных пользователя в его доменный объект
     *
     * @param userDto текущий шаблон данных пользователя
     * @return экземпляр класса {@link User}
     */

    public abstract User fromCreateUserDto(UserWithPasswordDto userDto);

    /**
     * Абстрактный метод для преобразования экземпляр класса {@link User} в DTO
     *
     * @param user текущий пользователь
     * @return экземпляр класса {@link UserDto}, шаблон данных для исходящего и входящего запроса
     */

    public abstract UserDto toUserDto(User user);
    public abstract List<UserDto> toUserDtos(List<User> user);
    @BeanMapping(nullValueCheckStrategy = ALWAYS, nullValuePropertyMappingStrategy = IGNORE)
    public abstract void copyForUpdate(UserDto request, @MappingTarget User user);
}