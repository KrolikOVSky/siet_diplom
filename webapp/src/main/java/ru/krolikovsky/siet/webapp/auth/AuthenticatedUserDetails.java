package ru.krolikovsky.siet.webapp.auth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * Класс {@code AuthenticatedUserDetails} для передачи данных пользователя в Spring Security
 */

@Data
@AllArgsConstructor
@Builder
public class AuthenticatedUserDetails implements UserDetails {
    private String id;
    private String password;
    private String username;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private Collection<? extends GrantedAuthority> authorities;
    private boolean credentialsNonExpired;
    private boolean enabled;
}
