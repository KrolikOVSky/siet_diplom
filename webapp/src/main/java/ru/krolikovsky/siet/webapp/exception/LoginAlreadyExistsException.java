package ru.krolikovsky.siet.webapp.exception;

import ru.krolikovsky.siet.common.SietException;

/**
 * Класс-исключение предназначенный для оповещения о том что пользователь уже существует
 */

public class LoginAlreadyExistsException extends SietException {

    /**
     * Конструктор {@code LoginAlreadyExistsException} для реализации исключения со следующим сообщением
     * <ul>
     *  <li>
     *      <i>
     *          "Пользователь с логином { логин пользователя } уже существует"
     *      </i>
     *  </li>
     * </ul>
     * @param login  имя пользователя, для которого произошло исключение
     */
    public LoginAlreadyExistsException(String login) {
        super(String.format("Пользователь с логином %s уже существует", login));
    }
}
