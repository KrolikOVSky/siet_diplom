package ru.krolikovsky.siet.webapp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.krolikovsky.siet.webapp.model.User;

import java.util.List;
import java.util.UUID;

/**
 * Интерфейс {@code UserRepository} служит для взаимодействия с сущностью {@link User Пользователи}
 */

public interface UserRepository extends JpaRepository<User, UUID> {
    /**
     * Метод выполняет поиск пользователя по его "имени пользователя"
     *
     * @param username искомое имя пользователя
     * @return Метод возвращает объект {@link User}
     */
    User findByLogin(String username);

    Page<User> findByNameContainingIgnoreCase(String name, Pageable pageable);
    List<User> findByNameContainingIgnoreCase(String name);
}