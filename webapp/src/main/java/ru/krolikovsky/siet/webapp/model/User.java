package ru.krolikovsky.siet.webapp.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.Length;
import ru.krolikovsky.siet.common.model.BaseSoftDeletedEntity;
import ru.krolikovsky.siet.common.model.Role;
import ru.krolikovsky.siet.common.model.SietUser;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import static ru.krolikovsky.siet.common.util.UuidConverter.fromUuid;

/**
 * Класс {@code User} описывает сущность пользователей
 */

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "users")
@Data
@Where(clause = "is_deleted = false")
@ToString(exclude = {"password"})
public class User extends BaseSoftDeletedEntity implements SietUser {

    public String getIdAsString(){
        return fromUuid(getId());
    }

    @NotEmpty(message = "Введите имя")
    @Length(max = 256, message = "Имя должно быть не длиннее {max} символов")
    private String name;

    @NotEmpty(message = "Введите логин")
    @Length(max = 20, message = "Логин должен быть не длиннее {max} символов")
    private String login;

    @Enumerated(EnumType.STRING)
    private Role role;

    private String password;

    boolean isDeleted;

    boolean isDisabled;
    private int failedLoginAttemptsCount;

    public boolean isTransient(){
        return getId() == null;
    }
}