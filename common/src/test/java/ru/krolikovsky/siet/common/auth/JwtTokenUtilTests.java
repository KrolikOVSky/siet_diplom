package ru.krolikovsky.siet.common.auth;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;
import ru.krolikovsky.siet.common.model.Role;
import ru.krolikovsky.siet.common.model.SietUser;

import static org.junit.jupiter.api.Assertions.*;

public class JwtTokenUtilTests {

    private JwtTokenUtil jwtTokenUtil;

    @Test
    public void testValidateGeneratedToken(){
        SietUser user = UserMock.builder()
                .idAsString("abc-123")
                .role(Role.ADMIN)
                .login("userLogin").build();
        String accessToken = jwtTokenUtil.generateAccessToken(user);

        assertTrue(jwtTokenUtil.validate(accessToken));
    }

    @Test
    public void validateSpoiledToken (){
        assertFalse(jwtTokenUtil.validate("wrong token"));
    }

    @Test
    public void testGetTokenClaims() {
        SietUser user = UserMock.builder()
                .idAsString("abc-123")
                .login("userLogin")
                .role(Role.ADMIN)
                .build();
        String accessToken = jwtTokenUtil.generateAccessToken(user);

        assertEquals(user.getIdAsString(), jwtTokenUtil.getUserId(accessToken));
        assertEquals(user.getLogin(), jwtTokenUtil.getUserLogin(accessToken));
        assertEquals(true, jwtTokenUtil.isUserAuthenticated(accessToken));
    }

    @BeforeEach
    public void setUp(){
        jwtTokenUtil = new JwtTokenUtil();
        ReflectionTestUtils.setField(jwtTokenUtil, "jwtSecret", "zdtlD3JK56m6wTTgsNFsdfSdfwerfewq");
        ReflectionTestUtils.setField(jwtTokenUtil, "jwtIssuer", "siet.krolikovsky.ru");
        ReflectionTestUtils.setField(jwtTokenUtil, "jwtExpirationDays", 7);
    }
}
