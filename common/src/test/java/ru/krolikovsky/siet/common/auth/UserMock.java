package ru.krolikovsky.siet.common.auth;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import ru.krolikovsky.siet.common.model.Role;
import ru.krolikovsky.siet.common.model.SietUser;

@Getter
@Setter
@Builder
public class UserMock implements SietUser {
    private String idAsString;
    private Integer supersetUserId;
    private String login;
    private String name;
    private Role role;
}
