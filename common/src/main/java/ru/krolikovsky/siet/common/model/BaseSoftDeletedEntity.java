package ru.krolikovsky.siet.common.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.MappedSuperclass;

import static ru.krolikovsky.siet.common.model.BaseSoftDeletedEntity.IS_DELETED_COLUMN_NAME;

@EqualsAndHashCode(callSuper = true)
@MappedSuperclass
@Data
@Where(clause = IS_DELETED_COLUMN_NAME + " = false")
public class BaseSoftDeletedEntity extends BaseEntity {
    public static final String IS_DELETED_COLUMN_NAME = "is_deleted";

    private boolean isDeleted;
}