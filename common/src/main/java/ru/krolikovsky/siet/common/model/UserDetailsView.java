package ru.krolikovsky.siet.common.model;

import lombok.Getter;

import javax.persistence.*;
import java.util.UUID;

/**
 * Класс-сущность для описания основных параметров пользователя, необходимых для проверки авторизованности
 */

@Entity
@Table(name = "users", schema = "public")
@Getter
public class UserDetailsView {
    @Id
    protected UUID id;
    protected String name;
    protected String login;
    @Enumerated(EnumType.STRING)
    private Role role;
}
