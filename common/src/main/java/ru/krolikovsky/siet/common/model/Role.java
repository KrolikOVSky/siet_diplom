package ru.krolikovsky.siet.common.model;

public enum Role {
    ADMIN,
    STUDENT,
    TEACHER
}
