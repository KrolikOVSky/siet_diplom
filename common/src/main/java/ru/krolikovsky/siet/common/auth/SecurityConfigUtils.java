package ru.krolikovsky.siet.common.auth;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.stereotype.Component;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import ru.krolikovsky.siet.common.web.MarmelabUtils;

import javax.servlet.http.HttpServletResponse;

/**
 * Утилиты для конфигурирования настроек безопасности
 */

@Component
public class SecurityConfigUtils {
    @Value("${application.debugMode}")
    private Boolean debugMode;

    /**
     * Метод {@code corsFilter} для фильтрации общего доступа к ресурсам для разных источников
     *
     * @return возвращает экземпляр класса {@link CorsFilter}
     */
    public CorsFilter corsFilter(){
        UrlBasedCorsConfigurationSource source =
                new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        //config.setAllowCredentials(true); //only care about this if using cookies
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        if(debugMode) {
            config.addAllowedOrigin("*");
            config.addExposedHeader(HttpHeaders.AUTHORIZATION);
        }
        config.addExposedHeader(MarmelabUtils.TABLE_TOTAL_COUNT_HEADER);
        config.addExposedHeader(HttpHeaders.CONTENT_DISPOSITION);
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }

    /**
     * Метод реализует конфигурирование безопасности доступа к общим ресурсам
     * @param http входящий http запрос
     * @return исходящий http запрос
     * @throws Exception
     */
    public HttpSecurity configureSharedSetting(HttpSecurity http) throws Exception {
        //Enable CORS and disable CSRF
        http = http.cors().and().csrf().disable();

        // Set session management to stateless
        http = http
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and();

        // Set unauthorized requests exception handler
        http = http
                .exceptionHandling()
                .authenticationEntryPoint(
                        (request, response, ex) -> {
                            response.sendError(
                                    HttpServletResponse.SC_UNAUTHORIZED,
                                    ex.getMessage()
                            );
                        }
                )
                .and();

        return http;
    }
}
