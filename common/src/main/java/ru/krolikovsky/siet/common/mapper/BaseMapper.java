package ru.krolikovsky.siet.common.mapper;

import ru.krolikovsky.siet.common.util.UuidConverter;

import java.util.UUID;

public abstract class BaseMapper {
    public String fromUUID(UUID uuid){
        return UuidConverter.fromUuid(uuid);
    }

    public UUID toUUID(String uuid){
        return UuidConverter.toUuid(uuid);
    }
}
