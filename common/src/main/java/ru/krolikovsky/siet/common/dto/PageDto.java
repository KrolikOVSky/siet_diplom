package ru.krolikovsky.siet.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * Класс {@code PageDto} для шаблона разрешенных входящих и исходящих данных страниц данных
 */

@Data
@AllArgsConstructor
public class PageDto <TDto>{
    private List<TDto> dtos;
    private long total;
}
