package ru.krolikovsky.siet.common.model;

/**
 * Интерфейс описывающий методы для взаимодействия с пользователем
 */

public interface SietUser {

    String getIdAsString();
    String getLogin();
    String getName();
    Role getRole();
}
