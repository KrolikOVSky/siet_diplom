package ru.krolikovsky.siet.common.authorization;

/**
 * Интерфейс описывающий поля ролей пользователя
 */
public interface RoleName {
    /**
     * Константа для описания прав администратора
     */
    String ADMIN = "ADMIN";
    /**
     * Константа для описания прав обучающегося
     */
    String STUDENT = "STUDENT";
    /**
     * Константа для описания прав преподавателя
     */
    String TEACHER = "TEACHER";

}