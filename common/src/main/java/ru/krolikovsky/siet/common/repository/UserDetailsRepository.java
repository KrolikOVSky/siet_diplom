package ru.krolikovsky.siet.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.krolikovsky.siet.common.model.UserDetailsView;

import java.util.UUID;


/**
 * Интерфейс {@code UserDetailsRepository} служит для взаимодействия с сущностью {@link UserDetailsView}
 */

public interface UserDetailsRepository extends JpaRepository<UserDetailsView, UUID> {
}
