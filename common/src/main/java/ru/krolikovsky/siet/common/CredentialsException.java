package ru.krolikovsky.siet.common;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.BadCredentialsException;

@Getter
@Setter
public class CredentialsException extends BadCredentialsException {
    private int retryCounts;

    public CredentialsException() {
        super("Неверный логин или пароль.");
    }

    public CredentialsException(int retryCounts) {
        super(String.format("Неверный логин или пароль. У вас осталось попыток: %d", Constants.MAX_FAILED_LOGIN_ATTEMPTS_COUNT - retryCounts));
        this.retryCounts = retryCounts;
    }
}
