package ru.krolikovsky.siet.common.authorization;

import org.springframework.stereotype.Component;
import ru.krolikovsky.siet.common.model.Role;
import ru.krolikovsky.siet.common.repository.UserDetailsRepository;
import ru.krolikovsky.siet.common.util.UuidConverter;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * Класс проверяет что пользователь существует и что у него есть необходимые роли
 * Используется в модулях, где достаточно иметь readonly доступ к пользователям
 */

@Component
public class RemoteUserValidatorImpl implements UserValidator {
    @Resource
    private UserDetailsRepository userDetailsRepository;

    @Override
    public boolean isUserActive(String userId) {
        return userDetailsRepository
                .findById(Objects.requireNonNull(UuidConverter.toUuid(userId)))
                .isPresent();
    }

    @Override
    public boolean isUserHasRole(String userId, Role role){
        return userDetailsRepository.findById(Objects.requireNonNull(UuidConverter.toUuid(userId)))
                .map(userDetailsView -> userDetailsView.getRole().equals(role))
                .orElse(false);
    }

}
