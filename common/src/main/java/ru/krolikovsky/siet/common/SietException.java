package ru.krolikovsky.siet.common;

/**
 * Исключение при возникновении ошибок во время работы с SIET
 */

public class SietException extends RuntimeException{

    public SietException(String message, Throwable cause){
        super(message, cause);
    }
    public SietException(String message) {
        super(message);
    }
}
