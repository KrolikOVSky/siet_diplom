package ru.krolikovsky.siet.common.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.krolikovsky.siet.common.CredentialsException;
import ru.krolikovsky.siet.common.SietException;
import ru.krolikovsky.siet.common.dto.ErrorDto;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.stream.Collectors;

/**
 * Класс-контроллер содержащий общие методы контроллеров приложения
 */

@ControllerAdvice
@Slf4j
public class AdviceController {

    @Value("${application.debugMode}")
    private Boolean debugMode;


    /**
     * Обработчик исключения SIET
     *
     * @param e исключение, которое необходимо обработать
     * @return исключение при работе с информационной системой
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(SietException.class)
    @ResponseBody
    public ErrorDto handleSietException(SietException e) {
        return logError(new ErrorDto(e.getMessage()));
    }

    /**
     * Обработчик стандартных исключений SIET
     *
     * @param exception исключение, которое необходимо обработать
     * @return исключение при работе с информационной системой
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public ErrorDto handleException(MethodArgumentNotValidException exception) {
        return logError(new ErrorDto(exception.getFieldErrors()
                .stream()
                .map(FieldError::getDefaultMessage)
                .collect(Collectors.joining(", "))));
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ErrorDto handleException(Exception ex) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);

        return logError(new ErrorDto(ex.getMessage(), debugMode ? getExceptionStackTraceAsString(ex) : ""));
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(DisabledException.class)
    public ErrorDto handleUserDisabledException() {
        return logError(new ErrorDto(ErrorDto.MessageCode.USER_DISABLED));
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(AccessDeniedException.class)
    public ErrorDto handleAccessDeniedError(AccessDeniedException exception) {
        return logError(
                new ErrorDto(exception.getMessage(), debugMode ? getExceptionStackTraceAsString(exception) : "")
        );
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(CredentialsException.class)
    public ErrorDto handleBadCredentials(CredentialsException credentialsException) {
        return logError(
                new ErrorDto(credentialsException.getMessage(), ErrorDto.MessageCode.BAD_CREDENTIALS, null)
        );
    }

    private ErrorDto logError(ErrorDto errorDto) {
        log.error("Ошибка при обработке запроса: {}, {}, {}", errorDto.getMessage(),
                errorDto.getMessageCode(), errorDto.getDetails());
        return errorDto;
    }

    private String getExceptionStackTraceAsString(Exception exception) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);
        return sw.toString();
    }
}
