package ru.krolikovsky.siet.common.util;

import org.springframework.util.StringUtils;

import java.util.UUID;

public final class UuidConverter {
    public static String fromUuid(UUID uuid) {
        return null != uuid ? uuid.toString() : "";
    }

    public static UUID toUuid(String uuid) {
        return StringUtils.hasText(uuid) ? UUID.fromString(uuid) : null;
    }
}
