package ru.krolikovsky.siet.common;

public class Constants {
    public static final String API_PATH_PREFIX = "siet-api";
    public static final String API_PATH_PREFIX_WEBAPP = API_PATH_PREFIX + "/webapp";
    public static final String API_PATH_PREFIX_SIET_MODULE = API_PATH_PREFIX + "/siet-module";
    public static final int MAX_FAILED_LOGIN_ATTEMPTS_COUNT = 3;
}
