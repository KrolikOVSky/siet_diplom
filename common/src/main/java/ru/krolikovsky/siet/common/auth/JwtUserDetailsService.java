package ru.krolikovsky.siet.common.auth;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Сервис для получения пользователя по его токену
 */

@Component
public class JwtUserDetailsService {

    @Resource
    private JwtTokenUtil jwtTokenUtil;

    /**
     * Метод дешифрует информацию о пользователе из его токена
     *
     * @param jwtToken полученных токен
     * @return информацию пользователя с его токеном
     * @throws UsernameNotFoundException если пользователь не найден в системе по данному токену
     */
    public JwtUserDetails loadUserByToken(String jwtToken) throws UsernameNotFoundException {
        return new JwtUserDetails(jwtTokenUtil.getUserId(jwtToken), jwtTokenUtil.getUserLogin(jwtToken),
                jwtTokenUtil.getUserRole(jwtToken));
    }
}
