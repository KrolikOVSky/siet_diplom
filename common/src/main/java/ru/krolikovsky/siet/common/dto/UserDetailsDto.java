package ru.krolikovsky.siet.common.dto;


import lombok.Data;

/**
 * Класс {@code UserDetailsDto} для шаблона разрешенных входящих и исходящих данных пользователя
 */
@Data
public class UserDetailsDto {
    private String id;
    private String name;
    private String login;
}
