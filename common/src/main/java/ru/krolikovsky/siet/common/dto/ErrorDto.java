package ru.krolikovsky.siet.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Класс {@code ErrorDto} для шаблона разрешенных входящих и исходящих данных ошибок при работе с информационной системой
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorDto {

    @AllArgsConstructor
    public enum MessageCode {
        USER_DISABLED,
        BAD_CREDENTIALS,
    }

    private String message;
    private MessageCode messageCode;
    private String details;

    public ErrorDto(MessageCode messageCode) {
        this(null, messageCode,  null);
    }

    public ErrorDto(String message) {
        this(message, null);
    }

    public ErrorDto(String message, String details) {
        this(message, null,  details);
    }
}
