package ru.krolikovsky.siet.common.authorization;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Сервис проверки прав пользователя
 */

@Service
public class PermissionService {


    /**
     * Метод проверяет роль авторизованного пользователя
     *
     * @param roleName роль, которую необходимо проверить у пользователя
     * @throws AccessDeniedException если ваша сессия не активна, либо у вас отсутствуют необходимые права
     */
    public void checkRole(String roleName){
        if(!isUserHasRole(roleName)){
            throw new AccessDeniedException(String.format("You do not have %s role to access this resource", roleName));
        }
    }

    /**
     * Метод проверяет есть ли у пользователя роль "Администратор"
     *
     * @return <ul>
     *      <li> true - если у данного пользователя есть роль "Администратор"
     *      <li> false - если у данного пользователя нет роли "Администратор"
     * </ul>
     */
    public boolean isAdmin() {
        return isUserHasRole(RoleName.ADMIN);
    }

    /**
     * Метод проверяет есть ли у пользователя указанная роль
     *
     * @param roleName роль, которую необходимо проверить у пользователя
     * @return <ul>
     *      <li> true - если у данного пользователя есть роль "Администратор"
     *      <li> false - если у данного пользователя нет роли "Администратор"
     * </ul>
     * @throws AccessDeniedException если ваша сессия не активна
     */
    private boolean isUserHasRole(String roleName) {
        return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
                .map(auth -> auth.getAuthorities()
                        .stream()
                        .anyMatch(a -> a.getAuthority().equalsIgnoreCase(roleName)))
                .orElseThrow(() -> new AccessDeniedException("Your session is not valid"));
    }
}
