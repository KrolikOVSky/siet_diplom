package ru.krolikovsky.siet.common.web;

import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateTimeParser {

    public static final String DATE_TIME_PATTERN = "yyyy-MM-dd'T'hh:mm:ss";
    public static final String DATE_PATTERN = "yyyy-MM-dd";

    public static Date parseDateTimeString(String dateString, String dateFormatPattern) throws ParseException {
        var dateFormat = new SimpleDateFormat(dateFormatPattern);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return StringUtils.hasText(dateString)
                ? dateFormat.parse(dateString)
                : null;
    }

}
