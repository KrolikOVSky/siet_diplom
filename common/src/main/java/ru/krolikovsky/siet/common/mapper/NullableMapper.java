package ru.krolikovsky.siet.common.mapper;

import org.mapstruct.Mapper;

import java.util.Optional;

@Mapper(componentModel = "spring")
public interface NullableMapper {

    default <T> Optional<T> wrap(T entity) {
        return Optional.ofNullable(entity);
    }

    default <T> T unwrap(Optional<T> value) {
        return value == null ? null : value.orElse(null);
    }

}
