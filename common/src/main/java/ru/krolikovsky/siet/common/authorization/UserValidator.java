package ru.krolikovsky.siet.common.authorization;

import ru.krolikovsky.siet.common.model.Role;

/**
 * Интерфейс описывающий методы для взаимодействия с проверкой данных пользователя
 */

public interface UserValidator {

    /**
     * Метод проверяет, существует ли такой пользователь в данный момент
     *
     * @param userId идентификационный номер пользователя
     * @return
     * <ul>
     *     <li> true - если пользователь авторизован
     *     <li> false - если пользователь не авторизован
     * </ul>
     *
     */
    boolean isUserActive(String userId);

    /**
     * Метод проверяет наличие определенной роли у пользователя
     *
     * @param userId идентификационный номер пользователя
     * @param role роль, которую необходимо проверить у пользователя
     * @return
     * <ul>
     *     <li> true - если у пользователя имеется запрошенная роль
     *     <li> false - если у пользователя отсутствует запрошенная роль
     * </ul>
     */
    boolean isUserHasRole(String userId, Role role);
}
