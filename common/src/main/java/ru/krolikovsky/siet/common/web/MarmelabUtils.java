package ru.krolikovsky.siet.common.web;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;

/**
 * Утилиты для работы с приложением React-Admin построенного на основе Marmelab
 * <p><a href=https://marmelab.com/react-admin/>Marmelab Copyright</a></p>
 */

public class MarmelabUtils {

    public static final String TABLE_TOTAL_COUNT_HEADER = "X-Total-Count";

    /**
     * Метод создает виртуальную страницу для внесения данных
     *
     * @param start начало страницы
     * @param end начало страницы
     * @return пустую страницу с указанными размерами
     */
    public static Pageable makePageable(int start, int end){
        int pageSize = end - start;
        return PageRequest.of(start / pageSize, pageSize);
    }

    /**
     * Метод создает метод сортировки по указанным параметрам
     *
     * @param sort поле, по которому необходимо сортировать
     * @param sortDirection направление сортировки
     * @return объект с заданными параметрами сортировки
     */
    public static Sort makeSortable(String sort, String sortDirection){
        if(!StringUtils.hasText(sort)){
            return null;
        }
        Sort.Direction direction = Sort.Direction.ASC;
        if(sortDirection.equalsIgnoreCase("DESC")){
            direction = Sort.Direction.DESC;
        }
        return Sort.by(direction, sort);
    }

    /**
     * Метод реализует запрос страницы по указанным параметрам
     *
     * @param start начало страницы
     * @param end начало страницы
     * @param sort поле, по которому необходимо сортировать
     * @param sortDirection направление сортировки
     * @return возвращает запрошенную страницу с заданными размером и методом сортировки
     */
    public static PageRequest makePageRequest(int start, int end, String sort, String sortDirection) {
        Pageable pageable = makePageable(start, end);
        Sort sortable = makeSortable(sort, sortDirection);

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sortable);
    }

    /**
     * Метод реализует запрос страницы по указанным параметрам
     *
     * @param start начало страницы
     * @param end начало страницы
     * @param sort правила сортировки
     * @return возвращает запрошенную страницу с заданными размером и методом сортировки
     */
    public static PageRequest makePageRequest(int start, int end, Sort sort) {
        Pageable pageable = makePageable(start, end);

        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
    }
}
