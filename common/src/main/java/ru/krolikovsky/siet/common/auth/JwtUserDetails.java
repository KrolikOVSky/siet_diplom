package ru.krolikovsky.siet.common.auth;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.krolikovsky.siet.common.model.Role;

import java.util.Collection;
import java.util.Collections;

/**
 * Класс для работы с данными пользователя, полученными по его токену
 */

public class JwtUserDetails implements UserDetails {

    private String username;
    private Role role;
    private String id;

    public JwtUserDetails(String id, String username, Role role){
        this.username = username;
        this.role = role;
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public Role getRole() {
        return role;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(
                new SimpleGrantedAuthority(role != null ? role.name(): "")
        );
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
