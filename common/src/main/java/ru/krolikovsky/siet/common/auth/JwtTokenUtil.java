package ru.krolikovsky.siet.common.auth;

import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.krolikovsky.siet.common.model.Role;
import ru.krolikovsky.siet.common.model.SietUser;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

/**
 * Утилиты для работы с токенами
 */

@Component
public class JwtTokenUtil {

    @Value("${jwt.secret}")
    private String jwtSecret;
    @Value("${jwt.issuer}")
    private String jwtIssuer;
    @Value("${jwt.ExpirationDays}")
    private int jwtExpirationDays;
    private final Logger logger = Logger.getLogger(JwtTokenUtil.class.getName());

    private final String CLAIM_NAME_USER_ID= "userId";
    private final String CLAIM_NAME_USER_LOGIN= "userLogin";
    private final String CLAIM_NAME_USER_ROLE= "userRole";
    private final String CLAIM_NAME_USER_FULL_NAME= "userFullName";
    private final String CLAIM_NAME_USER_AUTHENTICATED = "isAuthenticated";

    /**
     * Метод реализует генерирование токена пользователя
     *
     * @param user пользователь, для которого необходимо сгенерировать токен
     * @return готовый токен
     */
    public String generateAccessToken(SietUser user) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIM_NAME_USER_ID, user.getIdAsString());
        claims.put(CLAIM_NAME_USER_LOGIN, user.getLogin());
        claims.put(CLAIM_NAME_USER_ROLE, user.getRole().name());
        claims.put(CLAIM_NAME_USER_FULL_NAME, user.getName());
        claims.put(CLAIM_NAME_USER_AUTHENTICATED, Boolean.toString(true));
        return generateAccessToken(String.format("%s,%s", user.getIdAsString(), user.getLogin()), claims);
    }

    public String generateAnonymousAccessToken() {
        return generateAccessToken("anonymous", new HashMap<>());
    }

    /**
     * Метод извлекает идентификационный номер пользователя по его токену
     *
     * @param token токен пользователя
     * @return идентификационный номер пользователя
     */
    public String getUserId(String token) {
        return getClaim(token, CLAIM_NAME_USER_ID);
    }

    /**
     * Метод извлекает роль пользователя по его токену
     *
     * @param token токен пользователя
     * @return роль пользователя
     */
    public boolean isUserAuthenticated(String token) {
        return Boolean.parseBoolean(Optional.ofNullable(getClaim(token, CLAIM_NAME_USER_AUTHENTICATED))
                .orElse("false"));
    }

    /**
     * Метод извлекает логин пользователя по его токену
     *
     * @param token токен пользователя
     * @return логин пользователя
     */
    public String getUserLogin(String token) {
        return getClaim(token, CLAIM_NAME_USER_LOGIN);
    }

    /**
     * Метод возвращает дату истечения строка действия токена
     *
     * @param token токен пользователя
     * @return дата истечения строка действия токена
     */
    public Date getExpirationDate(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();

        return claims.getExpiration();
    }

    public Role getUserRole(String token) {
        String roleName = getClaim(token, CLAIM_NAME_USER_ROLE);
        return roleName != null ? Role.valueOf(roleName) : null;
    }

    /**
     * Проверка на валидность токена
     * @param token
     * @return
     * <ul>
     *     <li> true - токен прошел проверку
     *     <li> false - токен не верный
     * </ul>
     */
    public boolean validate(String token) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
            return !getExpirationDate(token).before(new Date());
        } catch (SignatureException ex) {
            logger.warning(String.format("Invalid JWT signature -%s}", ex.getMessage()));
        } catch (MalformedJwtException ex) {
            logger.warning(String.format("Invalid JWT token - %s", ex.getMessage()));
        } catch (UnsupportedJwtException ex) {
            logger.warning(String.format("Unsupported JWT token - %s", ex.getMessage()));
        } catch (IllegalArgumentException ex) {
            logger.warning(String.format("JWT claims string is empty - %s", ex.getMessage()));
        }
        return false;
    }

    private String getClaim(String token, String claimName) {
        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();

        return (String) claims.getOrDefault(claimName, null);
    }

    private String generateAccessToken(String subject, Map<String, Object> claims) {
        return Jwts.builder()
                .setSubject(subject)
                .setIssuer(jwtIssuer)
                .setClaims(claims)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + jwtExpirationDays * 24 * 60 * 60 * 1000)) // 1 week
                .signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
    }

}