package ru.krolikovsky.siet.common.auth;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Класс для получения аутентифицированного пользователя
 */

@Component
public class LoggedUserHolder {

    /**
     * Метод извлекает аутентифицированного
     * @return информацию пользователя с его токеном
     */
    public JwtUserDetails getAuthenticatedUserDetails() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!principal.getClass().isAssignableFrom(JwtUserDetails.class)) {
            //анонимный пользователь
            return null;
        }
        return (JwtUserDetails) principal;
    }
}
