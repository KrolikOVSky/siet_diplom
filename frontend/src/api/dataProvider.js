import jsonServerProvider from 'ra-data-json-server';
import httpClient, {sendPostData} from "./httpClient";
import {HTTP_METHOD, ROUTE_URLS} from "./constants";

const dataProvider = jsonServerProvider('', httpClient);

/**
 * кастомный dataProvider нужен для того, чтобы конвертировать файлы в base64
 * https://marmelab.com/react-admin/DataProviders.html#handling-file-uploads
 * @type {{[p: string]: any, deleteMany: (resource: string, params: DeleteManyParams) => Promise<DeleteManyResult>, updateMany: (resource: string, params: UpdateManyParams) => Promise<UpdateManyResult>, getList: <RecordType=Record extends Record>(resource: string, params: GetListParams) => Promise<GetListResult<RecordType>>, getMany: <RecordType=Record extends Record>(resource: string, params: GetManyParams) => Promise<GetManyResult<RecordType>>, getManyReference: <RecordType=Record extends Record>(resource: string, params: GetManyReferenceParams) => Promise<GetManyReferenceResult<RecordType>>, getOne: <RecordType=Record extends Record>(resource: string, params: GetOneParams) => Promise<GetOneResult<RecordType>>, update: ((function(*, *): (Promise<UpdateResult<Record>>))|*), create: <RecordType=Record extends Record>(resource: string, params: CreateParams) => Promise<CreateResult<RecordType>>, delete: <RecordType=Record extends Record>(resource: string, params: DeleteParams) => Promise<DeleteResult<RecordType>>}}
 */
const sietDataProvider = {
    ...dataProvider,
    update: (resource, params) => {
        return fileUploadCustom(resource, params, dataProvider.update, HTTP_METHOD.PUT);
    },
    create: (resource, params) => {
        return fileUploadCustom(resource, params, dataProvider.create, HTTP_METHOD.POST);
    }
}

/**
 * Convert a `File` object returned by the upload input into a base 64 string.
 * That's not the most optimized way to store images in production, but it's
 * enough to illustrate the idea of data provider decoration.
 */

export default sietDataProvider;

const fileUploadCustom = (resource, params, fallbackFunction, method) => {
    if (!isFileUploadAction(resource)) {
        return fallbackFunction(resource, params);
    }
    const {image, audio, ...data} = params.data
    return sendPostData(`/${resource}${method === HTTP_METHOD.PUT ? `/${params.id}` : ''}`, data, image?.rawFile, audio?.rawFile, method)
}


const isFileUploadAction = (resource) => {
    return [ROUTE_URLS.DASHBOARD].includes(resource);
}
