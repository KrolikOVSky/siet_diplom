import pageNaming from "../pageNaming";

export const LOCAL_STORAGE_AUTH = 'auth';
export const LOCAL_STORAGE_IS_REQUIRE_LOGIN = 'require-login'

export const API_PATH_WEBAPP = 'webapp';
export const API_PATH_PREFIX_SIET_MODULE = 'siet-module';

export const AUTH_RESPONSE = {
    OK: 'OK',
    BAD_CREDENTIALS: 'BAD_CREDENTIALS'
}

export const ROUTE_URLS = {
    USERS: `${API_PATH_WEBAPP}/users`,
    SUBJECTS: `${API_PATH_PREFIX_SIET_MODULE}/subjects`,
    HOME: '/',
    DASHBOARD: `home`,
    LOGIN: `${API_PATH_WEBAPP}/auth/login`,
    LOGOUT: '/logout',
    SIGN_UP: `${API_PATH_WEBAPP}/auth/sign-up`
}

export const applicationName = "Web-сервис выбора индивидуальных образовательных траекторий"
export const PAGE_NAMES = {
    USERS_LIST: `Список научных сотрудников`,
    USER_CREATE: `Новый пользователь`,
    USER: `Пользователь`,
    SUBJECTS_LIST: pageNaming.subject.list.header,
    SUBJECT_CREATE: pageNaming.subject.create.header,
    SUBJECT: pageNaming.subject.show.header,
    LOGIN: `${applicationName} | Вход`,
    SIGN_UP: `${applicationName} | регистрация`,
}

export const USER_ROLE = {
    ADMIN: 'ADMIN',
    TEACHER: 'TEACHER',
    STUDENT: 'STUDENT',
}

export const USER_ROLE_CHOICES = [
    {id: USER_ROLE.ADMIN, name: 'Администратор'},
    {id: USER_ROLE.TEACHER, name: 'Преподаватель'},
    {id: USER_ROLE.STUDENT, name: 'Обучающийся'},
];

export const HTTP_METHOD = {
    GET: 'GET',
    POST: 'POST',
    PUT: 'PUT',
    DELETE: 'DELETE'
}
