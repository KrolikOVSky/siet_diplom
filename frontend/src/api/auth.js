import {anonymousQuery} from './httpClient';
import {ROUTE_URLS} from "./constants";

export const login = (login, password) => {
    return anonymousQuery.post('/' + ROUTE_URLS.LOGIN,
        {
            'login': login,
            'password': password
        }
    );
}