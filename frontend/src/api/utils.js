import {fetch} from "./httpClient";

export const setPageName = page => {
    document.title = page
}

export const getFileExtension = (path) => {
    return path && path.split('.').pop();
}

export const formatFileSize = size => {
    const roundNumber = num => Math.round((num + Number.EPSILON) * 100) / 100
    return size <= 1024
        ? `${size} байт`
        : size <= 1024 * 1024 && size > 1024
            ? `${roundNumber(size / 1024)} кб`
            : size <= 1024 * 1024 * 1024 && size > 1024 * 1024
                ? `${roundNumber(size / 1024 / 1024)} мб`
                : `${roundNumber(size / 1024 / 1024 / 1024)} гб`
}

const parseFileNameFromContentDisposition = (disposition) => {
    let filename = "";
    if (disposition && disposition.indexOf('attachment') !== -1) {
        const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        const matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) {
            filename = matches[1].replace(/['"]/g, '');
        }
    }
    return filename;
}

export const fetchFile = (urlPrefix, id) => {
    return fetch(urlPrefix + '/' + id, {
        responseType: 'blob'
    }).then(({data, headers}) => {
        const fileName = parseFileNameFromContentDisposition(headers['content-disposition'])
        return {
            file: URL.createObjectURL(data),
            arrayBufferPromise: data.arrayBuffer(),
            fileName
        }
    })
}
