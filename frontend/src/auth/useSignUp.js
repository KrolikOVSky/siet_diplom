import {useCallback} from "react";
import {fetch} from "../api/httpClient";
import {HTTP_METHOD, ROUTE_URLS} from "../api/constants";

export const useSignUp = () => {
    return useCallback((params) => {
        return fetch(`/${ROUTE_URLS.SIGN_UP}`, {
            method: HTTP_METHOD.POST,
            data: params
        })
    }, [])
}
