export const API_PATH_PREFIX = "siet-api"
export const AUTHENTICATION_SERVICE_HOST = process.env.NODE_ENV === 'development' ? `http://localhost:8080/${API_PATH_PREFIX}` : API_PATH_PREFIX;
export const SIET_MODULE_SERVICE_HOST = process.env.NODE_ENV === 'development' ? `http://localhost:8081/${API_PATH_PREFIX}` : API_PATH_PREFIX;
