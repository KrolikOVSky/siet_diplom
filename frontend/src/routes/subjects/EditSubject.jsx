import {Edit} from "react-admin";
import pageNaming from "../../pageNaming";
import {PageTitle} from "../../components/PageTitle";
import React from "react";
import {Container} from "@mui/material";
import {EditToolbar} from "../../components/EditToolbar";
import FormSubject from "./FormSubject";

export const EditSubject = (props) => {
    return <Container>
        <PageTitle header={pageNaming.subject.edit.header}
                   description={pageNaming.subject.edit.description}
        />
        <Edit {...props}
              mutationMode="pessimistic"
        >
            <FormSubject {...props} isNew={false} toolbar={<EditToolbar/>}/>
        </Edit>
    </Container>
}