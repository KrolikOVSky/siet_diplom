import * as React from "react";
import {BulkDeleteWithConfirmButton, Datagrid, DateField, List, SearchInput, TextField} from 'react-admin';
import {ResearchToolkitPagination} from "../../components/Pagination";
import {Box} from '@mui/material';
import EmptyPage from "../../components/EmptyPage";
import {PageTitle} from "../../components/PageTitle";
import pageNaming from "../../pageNaming";

export const ListSubjects = props => {
    const filters = [
        <SearchInput
            source="title"
            alwaysOn
        />
    ];

    return <Box>
        <PageTitle header={pageNaming.subject.list.header}/>
        <List {...props}
              title={pageNaming.subject.list.header}
              bulkActionButtons={
                  <BulkDeleteWithConfirmButton
                      confirmTitle={'Удалить учебные предметы'}
                      confirmContent={'Вы действительно хотите удалить выбранные учебные предметы?'}
                  />
              }
              exporter={false}
              pagination={<ResearchToolkitPagination/>}
              empty={<EmptyPage header='Нет учебных предметов'
                                description='Хотите добавить новый учебный предмет?'/>}
              filters={filters}
        >
            <Datagrid rowClick="edit">
                <TextField source="title" label="Название"/>
                <DateField source="createDate" label="Дата создания" locales='ru-RU' showTime/>
                <DateField source="lastUpdate" label="Дата последнего изменения" locales='ru-RU' showTime/>
            </Datagrid>
        </List>
    </Box>;
}
