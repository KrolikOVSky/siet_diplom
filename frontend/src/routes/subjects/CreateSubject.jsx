import {Create, useRedirect} from "react-admin";
import {Container} from "@mui/material";
import pageNaming from "../../pageNaming";
import {PageTitle} from "../../components/PageTitle";
import React from "react";
import {ROUTE_URLS} from "../../api/constants";
import {CreateToolbar} from "../../components/CreateToolbar";
import FormSubject from "./FormSubject";

export const CreateSubject = (props) => {
    const redirect = useRedirect()
    return <Container>
        <PageTitle header={pageNaming.subject.create.header}
                   description={pageNaming.subject.create.description}
        />
        <Create {...props}
                onSuccess={() => redirect(`/${ROUTE_URLS.SUBJECTS}`)}
        >
            <FormSubject {...props} isNew={true} toolbar={<CreateToolbar/>}/>
        </Create>
    </Container>

}
