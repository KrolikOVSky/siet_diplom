import {required, SimpleForm, TextInput} from "react-admin";
import React from "react";

const FormSubject = ({isNew, ...props}) => {
    return <SimpleForm {...props}>
        <TextInput
            source="title"
            validate={required()}
            options={{autoComplete: 'none'}}
            label="Название учебного предмета"
        />
        <TextInput
            source="description"
            options={{autoComplete: 'none'}}
            label="Описание учебного предмета"
        />
    </SimpleForm>
}

export default FormSubject;