import {Card, CardContent, CardHeader} from "@mui/material";
import {USER_ROLE} from "../api/constants";

export const Dashboard = ({...props}) => {
    const {isAdmin = false, isTeacher = false, isStudent = false} = props?.permissions || {isAdmin: false, isTeacher: false, isStudent: false}
    const outputRole = isAdmin
        ? USER_ROLE.ADMIN
        : isTeacher
            ? USER_ROLE.TEACHER
            : isStudent
                ? USER_ROLE.STUDENT
                : 'Anonymous'
    return <Card>
        <CardHeader title={`Welcome to the administration '${outputRole}'`}/>
        <CardContent>Lorem ipsum sic dolor amet...</CardContent>
    </Card>
};
