import {Route} from "react-router-dom";
import * as React from "react";
import LogoutPage from "../components/authPages/logout/LogoutPage";
import {Dashboard} from "./Dashboard";
import AuthPage from "../components/authPages/login/AuthPage";

const customRoutes = [
    <Route noLayout path="/logout" render={() =>
        <LogoutPage/>
    }/>,
    <Route noLayout path="/sign-up" render={(props) => <AuthPage signUp {...props}/>}/>,
    <Route exact path="/dashboard"
           component={Dashboard}
           noLayout
    />
]
export default customRoutes;
