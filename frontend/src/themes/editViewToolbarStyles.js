import {makeStyles} from '@material-ui/core/styles';

export const getToolbarStyles = makeStyles({
    toolbar: {
        display: 'flex',
        justifyContent: 'space-between',
        borderRadius: '0 0 4px 4px'
    }
});