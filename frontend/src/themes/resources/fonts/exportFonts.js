import RobotoRegular from "./Roboto-Regular.ttf"

export const robotoRegular = {
    fontFamily: '"Roboto"',
    fontStyle: 'normal',
    fontDisplay: 'swap',
    fontWeight: '400',
    src: `local("Roboto-Regular"), local("Roboto-Regular.ttf"), url(${RobotoRegular}) format("ttf")`
}
