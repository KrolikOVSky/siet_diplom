const pageNaming = {
    users: {
        list: {
            header: 'Список научных сотрудников',
            description: 'Вы можете просматривать список всех сотрудников в Инструменте, ' +
                'редактировать их персональные данные, добавлять новых сотрудников, ' +
                'а также осуществлять поиск по учетным записям'
        },
        create: {
            header: 'Создание новой учетной записи',
            description: `Вы можете добавить сотрудника в SIET`
        },
        edit: {
            header: 'Настройки учетной записи',
            description: 'Вы можете редактировать персональные данные, заблокировать или удалить сотрудника'
        },
        show: {
            header: 'Личный кабинет научного сотрудника',
            description: 'Вы можете просмотреть свои персональные данные и при необходимости изменить пароль'
        }
    },
    subject: {
        list: {
            header: 'Учебные предметы',
            description: ''
        },
        create: {
            header: 'Создать учебный предмет',
            description: ''
        },
        edit: {
            header: 'Редактировать учебный предмет',
            description: ''
        },
        show: {
            header: 'Учебный предмет',
            description: ''
        }
    },
    template: {
        list: {
            header: '',
            description: ''
        },
        create: {
            header: '',
            description: ''
        },
        edit: {
            header: '',
            description: ''
        },
        show: {
            header: '',
            description: ''
        }
    },
}
export default pageNaming;
