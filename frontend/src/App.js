import * as React from "react";
import {HashRouter} from "react-router-dom";
import authProvider from "./auth/authProvider";
import {Admin, Resource} from 'react-admin';
import sietDataProvider from "./api/dataProvider";
import {mainTheme} from "./themes/mainTheme";
import polyglotI18nProvider from 'ra-i18n-polyglot';
import messages from "./messages";
import SietLayout from "./components/layout/SietLayout";
import AuthPage from "./components/authPages/login/AuthPage";
import {routeCurrentUser, routeManageSubjects, routeManageUsers} from "./routes";
import {Dashboard} from "./routes/Dashboard";
import {ROUTE_URLS} from "./api/constants";
import {Dashboard as DashboardIcon} from "@mui/icons-material";
import customRoutes from "./routes/customRoutes";


const i18nProvider = polyglotI18nProvider(() => messages, 'ru', {allowMissing: true});

const initialState = {
    admin: {
        ui: {
            sidebarOpen: authProvider.isAdmin(),
            viewVersion: 0
        }
    }
}
const App = () => {
    return <HashRouter>
        <Admin dataProvider={sietDataProvider}
               customRoutes={customRoutes}
               authProvider={authProvider}
               initialState={initialState}
               i18nProvider={i18nProvider}
               theme={mainTheme}
               appLayout={SietLayout}
               logoutButton={false}
               loginPage={AuthPage}
               disableTelemetry
        >
            {permissions => [
                <Resource name={ROUTE_URLS.DASHBOARD}
                          options={{label: 'DASHBOARD'}}
                          list={props => {
                              return Dashboard(props)
                          }}
                          icon={DashboardIcon}
                />,
                ...permissions.isAdmin ?
                    [
                        ...routeManageUsers(),
                        ...routeManageSubjects()
                    ] : [],
                ...permissions.isTeacher ?
                    [] : [],
                ...permissions.isStudent ?
                    [] : [],
                ...!permissions.isAdmin ? [
                    //чтобы анонимному пользователю открывалась страница логина необходимо чтобы первым в списке ресурсов был "login"
                    <Resource name="login"/>,
                    ...routeCurrentUser()
                ] : []
            ]}
        </Admin>
    </HashRouter>
}

export default App;
