import * as React from "react";
import {Resource} from "react-admin";
import {PAGE_NAMES, ROUTE_URLS} from "./api/constants";
import {setPageName} from "./api/utils";
import {EditUser} from "./routes/users/EditUser";
import {ListUsers} from "./routes/users/ListUsers";
import {CreateUser} from "./routes/users/CreateUser";
import {UsersIcon} from "./icons/iconsFactory";
import {Dashboard} from "./routes/Dashboard";
import {Dashboard as DashboardIcon} from "@mui/icons-material"
import {EditSubject} from "./routes/subjects/EditSubject";
import {ListSubjects} from "./routes/subjects/ListSubjects";
import {CreateSubject} from "./routes/subjects/CreateSubject";

export const routeManageUsers = () => [
    <Resource name={ROUTE_URLS.USERS}
              options={{label: PAGE_NAMES.USERS_LIST}}
              edit={props => {
                  setPageName(PAGE_NAMES.USER)
                  return EditUser({...props})
              }}
              list={props => {
                  setPageName(PAGE_NAMES.USERS_LIST)
                  return ListUsers(props)
              }}
              create={props => {
                  setPageName(PAGE_NAMES.USER_CREATE)
                  return CreateUser(props)
              }}
              icon={UsersIcon}
    />
];

export const routeCurrentUser = () => [
    <Resource name={ROUTE_URLS.USERS}
              edit={props => {
                  setPageName(PAGE_NAMES.USER)
                  return EditUser({...props})
              }}/>
]

export const routeDashboard = () => [
    <Resource name={ROUTE_URLS.DASHBOARD}
              list={props => {
                  return Dashboard(props)
              }}
              icon={DashboardIcon}
    />
]

export const routeManageSubjects = () => [
    <Resource name={ROUTE_URLS.SUBJECTS}
              options={{label: PAGE_NAMES.SUBJECTS_LIST}}
              edit={props => {
                  setPageName(PAGE_NAMES.SUBJECT)
                  return EditSubject({...props})
              }}
              list={props => {
                  setPageName(PAGE_NAMES.SUBJECTS_LIST)
                  return ListSubjects(props)
              }}
              create={props => {
                  setPageName(PAGE_NAMES.USER_CREATE)
                  return CreateSubject(props)
              }}
              icon={UsersIcon}
    />
]