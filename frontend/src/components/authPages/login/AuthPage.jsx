import * as React from 'react';
import {useState} from 'react';
import {Notification, useLogin, useNotify, useRedirect} from 'react-admin';
import {Box} from '@mui/material';
import {useAuthPagesStyles} from "../authPagesStyles";
import {TextInput} from "../../TextInput";
import {PasswordInput} from "../../PasswordInput";
import {AuthButton} from "../AuthButton";
import {setPageName} from "../../../api/utils";
import {PAGE_NAMES, ROUTE_URLS, USER_ROLE, USER_ROLE_CHOICES} from "../../../api/constants";
import {useSignUp} from "../../../auth/useSignUp";
import {SelectInput} from "../../SelectInput";

const AuthPage = (props) => {
    setPageName(PAGE_NAMES.LOGIN);

    const classes = useAuthPagesStyles();

    const {signUp} = props
    const [role, setRole] = useState('');
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);
    const notify = useNotify();
    const login = useLogin();
    const redirect = useRedirect()
    const signUpFunc = useSignUp()

    const handleLogin = () => {
        login({username, password}).then(() => {
            setLoading(false)
        }).catch((error) => {
            setLoading(false)
            notify(error, {type: 'warning'});
        })
    }

    const handleSubmit = e => {
        setLoading(true);
        e.preventDefault();
        handleLogin()
    };

    const handleAuthRedirect = () => {
        redirect(signUp ? 'login' : 'sign-up');
    }

    const handleSignUp = () => {
        setLoading(true)
        signUpFunc({login: username, email, password, name, role}).then(() => {
            handleLogin()
            setLoading(false)
        });
    };

    return (
        <Box className={classes.body}>
            <Box className={classes.login}>
                <form className={classes.form} onSubmit={handleSubmit}>
                    <Box className={classes.form}>
                        {signUp && <TextInput label={'Имя'}
                                              variant='standard'
                                              required
                                              onChange={e => setName(e.target.value)}
                        />}
                        {signUp && <SelectInput choices={USER_ROLE_CHOICES.filter(role => role.id !== USER_ROLE.ADMIN)}
                                                label="Роль"
                                                required
                                                onChange={e => setRole(e.target.value)}
                        />}
                        {signUp && <TextInput label={'Email'}
                                              variant='standard'
                                              required
                                              onChange={e => setEmail(e.target.value)}
                        />}
                        <TextInput label={signUp ? 'Имя пользователя' : 'Имя пользователя или email'}
                                   variant='standard'
                                   required
                                   onChange={e => setUsername(e.target.value)}
                        />
                        <PasswordInput label='Пароль'
                                       variant='standard'
                                       required
                                       type='password'
                                       onChange={e => setPassword(e.target.value)}
                        />
                    </Box>
                    {!signUp && <AuthButton type='submit'
                                            loading={loading}
                    >
                        Войти
                    </AuthButton>}

                    {signUp && <AuthButton type='submit'
                                           loading={loading}
                                           onClick={handleSignUp}
                    >
                        Зарегистрироваться
                    </AuthButton>}

                    <AuthButton type='button'
                                loading={loading}
                                onClick={handleAuthRedirect}
                    >
                        {signUp ? 'Уже есть аккаунт (Войти)' : 'Регистрация'}
                    </AuthButton>
                    <AuthButton type='button'
                                loading={loading}
                                onClick={() => redirect(ROUTE_URLS.DASHBOARD)}
                    >
                        Продолжить без {signUp ? 'авторизации' : 'регистрации'}
                    </AuthButton>
                </form>
            </Box>
            <Notification/>
        </Box>
    );
};
export default AuthPage

