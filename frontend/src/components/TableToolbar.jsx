import * as React from "react";
import {CreateButton, TopToolbar} from 'react-admin';
import {includes, keys} from "lodash";
import FilterButton from "./FilterButton";
import {PlusIcon} from "../icons/iconsFactory";
import {Box} from "@material-ui/core";
import {useTableToolbarStyles} from "./fliterStyles";

const TableToolbar = ({
                          showCreateButton = true, showFilterButton = true, filters = [],
                          customContent, ...restProps
                      }) => {
    const filterOptions = filters.filter(filter => !includes(keys(restProps.displayedFilters), filter.props.source));
    const tableToolbarStyles = useTableToolbarStyles()

    return <TopToolbar>
        <Box className={tableToolbarStyles.buttonsWrapper}>
            {
                showFilterButton && <FilterButton showFilter={restProps.showFilter}
                                                  filters={filterOptions}
                                                  className={tableToolbarStyles.buttons}
                />
            }
            {
                showCreateButton && <CreateButton icon={<PlusIcon/>}
                                                  className={tableToolbarStyles.buttons}
                />
            }
            {customContent}
        </Box>
    </TopToolbar>;
}

export default TableToolbar;