import * as React from "react";
import {Pagination} from 'react-admin';

export const ResearchToolkitPagination = ({isOpenFirstPage, setOpenFirstPage, ...props}) => {
    if (isOpenFirstPage) {
        setOpenFirstPage(false)
        props.setPage(1);
    }
    return <Pagination rowsPerPageOptions={[5, 10, 20, 50]} {...props}/>
}
