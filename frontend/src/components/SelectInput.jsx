import {FormControl, InputLabel, MenuItem, Select} from "@mui/material";
import {useState} from "react";

export const SelectInput = (props) => {
    const {choices, onChange, defaultValue, label} = props
    const [value, setValue] = useState(defaultValue)

    return <FormControl variant="standard">
        <InputLabel>{label}</InputLabel>
        <Select
            value={value}
            label={label}
            onChange={e => {
                setValue(e.target.value)
                onChange(e)
            }}
        >
            <MenuItem value="">
                <em>Выберите Роль</em>
            </MenuItem>
            {choices.map(item => <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>)}
        </Select>
    </FormControl>
}
