import {AppBar, MenuItemLink, useRefresh, UserMenu} from "react-admin";
import auth from "../../auth/authProvider";
import authProvider from "../../auth/authProvider";
import {Box} from '@material-ui/core';
import {useAppBarStyles} from "./appBarStyles";
import {RefreshIcon, UserIcon} from "../../icons/iconsFactory";
import {applicationName, ROUTE_URLS} from "../../api/constants";
import {IconButton} from "@mui/material";
import {makeStyles} from "@material-ui/core/styles";
import {LocalLibrary as LocalLibraryIcon} from '@mui/icons-material';
import {LogoutButton} from "../logoutButton/LogoutButton";

const useAppbarStyles = makeStyles(() => {
    if (authProvider.isAdmin()) {
        return {};
    }
    //для не админов скрываем кнопку управления левым меню
    return {
        menuButton: {
            display: 'none'
        }
    }
});

const RefreshButton = () => {
    const refresh = useRefresh();
    return <IconButton onClick={() => refresh()} color="inherit">
        <RefreshIcon/>
    </IconButton>;
};

const SietAppBar = (props) => {
    const classes = useAppBarStyles();
    const appbarClasses = useAppbarStyles();

    const SietUserMenu = (props) => {
        return <UserMenu icon={<UserIcon/>} {...props} styles={{border: 'solid 1px #fff'}}>
            {!authProvider.isAnonymous() ? <>
                    <MenuItemLink
                        to={`/${ROUTE_URLS.USERS}/${auth.getCurrentUserId()}`}
                        primaryText="Мой Профиль"
                    />
                    <LogoutButton/>
                </>
                : <MenuItemLink
                    to={`/login`}
                    primaryText="Войти"
                />}
        </UserMenu>;
    };

    return <AppBar {...props} userMenu={<SietUserMenu/>}>
        <a href={`/#/home`} className={classes.caption}>{applicationName}</a>
        {!authProvider.isAnonymous() && <Box>
            {authProvider.getCurrentUserName()}
        </Box>}
        <RefreshButton/>
    </AppBar>
};

export default SietAppBar;
