import React from "react";
import {Layout, Sidebar} from "react-admin";
import SietAppBar from "./SietAppBar";
import {makeStyles} from "@material-ui/core/styles";
import authProvider from "../../auth/authProvider";

const useSidebarStyles = makeStyles(() => {
    if(authProvider.isAdmin()){
        return {};
    }

    return {
        drawerPaper: {
            //для простых пользователей (не админов) скрываем левую паннель
            width: '0px'
        }
    }
});

const SietSidebar = props => {
    const classes = useSidebarStyles();
    return (
        <Sidebar {...props}/>
    );
};

const SietLayout = props =>
    <Layout {...props}
            sidebar={SietSidebar}
            appBar={SietAppBar}/>;
export default SietLayout;
