import {getToolbarStyles} from "../themes/editViewToolbarStyles";
import {FloppyIcon, TrashIcon} from "../icons/iconsFactory";
import {DeleteButton, SaveButton, Toolbar} from "react-admin";
import CancelButton from "./CancelButton";

export const EditToolbar = ({onSave, pristine, ...props}) => {
    return <Toolbar {...props} classes={getToolbarStyles()}>
        <SaveButton icon={<FloppyIcon/>}
                    label="Сохранить"
                    variant="text"
                    pristine={pristine}
                    onSave={onSave}
                    disabled={pristine}/>
        <CancelButton/>
        <DeleteButton icon={<TrashIcon/>}
                      mutationMode="pessimistic"
                      confirmTitle={'Удалить выбранные элементы?'}
                      onSuccess={props.onSuccess}
        />
    </Toolbar>
}