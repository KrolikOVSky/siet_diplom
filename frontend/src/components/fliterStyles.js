import {makeStyles} from "@material-ui/core/styles";

export const useTableToolbarStyles = makeStyles({
    buttonsWrapper: {
        display: "flex",
        alignItems: "right",
        justifyContent: "right",
        marginRight: "80px"
    },
    buttons: {
        color: "#929da6",
        "&:hover": {
            backgroundColor: 'transparent',
            color: '#253b4d'
        },
        "&:active": {
            backgroundColor: '#f5f6f7',
        }
    }
})

export const useSearchInputStyle = makeStyles({
    searchInput:{
        width: '272px',
        height: '42px',
        margin: '0 0 2rem 0'
    }
})