import {Box} from "@mui/material";
import ErrorOutlineIcon from "@mui/icons-material/ErrorOutline";
import BrokenImageIcon from "@mui/icons-material/BrokenImage";

const BrokenFile = ({errorMessage, defaultErrorMessage}) => {
        return <Box>
            <Box sx={{display: 'flex', alignItems: 'center', marginBottom: '10px'}}>
                <ErrorOutlineIcon sx={{marginRight: '7px'}}/>
                <Box>{errorMessage || defaultErrorMessage}</Box>
            </Box>
            <Box sx={{
                display: 'flex',
                justifyContent: 'space-around',
                backgroundColor: '#fafafa',
                borderRadius: '2px',
                border: '1px solid rgba(0,0,0,.3)',
                width: '100%',
                height: '91vh',
            }}>
                <BrokenImageIcon color="disabled" sx={{fontSize: '15vw', margin: 'auto'}}/>
            </Box>
        </Box>;
}

export default BrokenFile;