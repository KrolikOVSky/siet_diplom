import {TransformComponent, TransformWrapper} from "@pronestor/react-zoom-pan-pinch";
import {ControlCamera, RestartAlt, ZoomIn, ZoomOut} from "@mui/icons-material";
import {useImageViewerStyles} from "./styles";
import {IconButton} from "@mui/material";
import DownloadIcon from '@mui/icons-material/Download';
import doDownload from "../../fileDownload/fileDownload";
import {TIFFViewer} from "react-tiff";
import {usePermissions} from "react-admin";

export const ImageViewer = ({src, alt, classNameImage, classNameWrapper}) => {
    const classes = useImageViewerStyles();
    const {loaded, permissions} = usePermissions();

    const download = () => {
        doDownload(src, alt);
    }

    return <TransformWrapper maxScale={100} minScale={-20}>
        {({ zoomIn, zoomOut, resetTransform, centerView, ...rest }) => (
            <>
            <div>
                <IconButton title="Увеличить" onClick={() => zoomIn()}>
                    <ZoomIn />
                </IconButton>
                <IconButton title="Уменьшить" onClick={() => zoomOut()}>
                    <ZoomOut />
                </IconButton>
                <IconButton title="Поместить по центру" onClick={() => centerView()}>
                    <ControlCamera />
                </IconButton>
                <IconButton title="Сбросить настройки" onClick={() => resetTransform()}>
                    <RestartAlt />
                </IconButton>
                <IconButton title="Скачать" onClick={download}>
                    <DownloadIcon/>
                </IconButton>
            </div>
            <TransformComponent wrapperClass={classes.magnifier + ' ' + classNameWrapper}>
                {
                    alt.substring(alt.lastIndexOf('.') + 1).toLowerCase() === 'tiff'
                        ? <TIFFViewer tiff={src} className={classNameImage}/>
                        : <img src={src} alt={alt} className={classNameImage} style={{maxWidth: '100%'}}/>
                }
            </TransformComponent>
            </>
            )
        }
    </TransformWrapper>
}