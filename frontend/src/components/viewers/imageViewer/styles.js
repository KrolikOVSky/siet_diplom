import {makeStyles} from "@material-ui/core/styles";

export const useImageViewerStyles = makeStyles({
    magnifier: {
        width: '100%',
        cursor: 'all-scroll'
    }
})