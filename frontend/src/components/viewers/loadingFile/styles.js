import {makeStyles} from "@material-ui/core/styles";

export const useFileLoadingStyles = makeStyles({
    headingLoadingText: {
        paddingLeft: "25px",
        fontStyle: "italic"
    },
    fileLoadingContainer: {
        marginTop: '15px',
    },
})