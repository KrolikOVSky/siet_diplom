import {useFileLoadingStyles} from "./styles";
import {Box, CircularProgress, Skeleton} from "@mui/material";

export const FileLoading = () => {
    const classes = useFileLoadingStyles();

    return <Box className={classes.headingLoadingText}>
        <span>
            Файл загружается...
            <CircularProgress thickness={2}
                              size={20}
            />
        </span>
        <Skeleton className={classes.fileLoadingContainer}
                  variant="rectangular"
                  height="50vh"
        />
    </Box>
}