import {TextInput} from "./TextInput";
import {InputAdornment} from "@material-ui/core";
import {Search as SearchIcon} from "@mui/icons-material";
import {useSearchInputStyle} from "./fliterStyles";

export const SearchInput = ({...props}) => {
    const classes = useSearchInputStyle()
    return <TextInput {...props}
                      InputProps={{
                          endAdornment: (
                              <InputAdornment position={"end"}>
                                      <SearchIcon />
                              </InputAdornment>
                          )
                      }}
                      className={classes.searchInput + ' filter-field '}
    />
}