import {Logout, useLogout, useRedirect, useRefresh} from "react-admin";
import {LogoutIcon} from "../../icons/iconsFactory";
import {logoutStyles} from "./styles";
import {ROUTE_URLS} from "../../api/constants";
import {Box, Button, ListItemIcon, ListItemText, MenuItem} from "@mui/material";
import redirect from "react-router-dom/es/Redirect";

export const LogoutButton = props => {
    const styles = logoutStyles();
    const logout = useLogout();
    const redirect = useRedirect();
    return <MenuItem
        onClick={async () => {
            await logout();
            redirect(ROUTE_URLS.DASHBOARD)
        }}
        className={styles.logoutButton}
    >
        <ListItemIcon className={styles.logoutIcon}>
            <LogoutIcon color={'#929da6'} className={styles.logoutIcon}/>
        </ListItemIcon>
        <ListItemText className={styles.logoutText}>
            Выйти
        </ListItemText>
    </MenuItem>
}
