import {makeStyles} from "@material-ui/core";

const defaultColor = {
    color: '#929da6',
}

const hoverColor = {
    color: '#f5f6f7',
    // backgroundColor: 'rgba(26,41,54,1)'
}

export const logoutStyles = makeStyles(() => ({
    logoutButton: {
        ...defaultColor,
        "&:hover": {
            ...hoverColor,
            backgroundColor: 'rgb(26,41,54,1)!important'
        }
    },
    logoutText: {
        ...defaultColor,
        "&:hover": {
            ...hoverColor,
        }
    },
    logoutIcon: {
        ...defaultColor,
        "&:hover": {
            ...hoverColor,
        }
    }
}))
