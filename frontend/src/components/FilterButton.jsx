import * as React from "react";
import {MenuItem, MenuList, Paper} from '@mui/material';
import Button from '@material-ui/core/Button';
import Popper from '@mui/material/Popper';
import ClickAwayListener from '@mui/material/ClickAwayListener';
import Grow from '@mui/material/Grow';
import {FilterIcon} from "../icons/iconsFactory";

const FilterButton = ({showFilter, filters, className}) => {
    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef(null);

    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }

        setOpen(false);
    };

    function handleListKeyDown(event) {
        if (event.key === 'Tab') {
            event.preventDefault();
            setOpen(false);
        } else if (event.key === 'Escape') {
            setOpen(false);
        }
    }

    // return focus to the button when we transitioned from !open -> open
    const prevOpen = React.useRef(open);
    React.useEffect(() => {
        if (prevOpen.current === true && open === false) {
            anchorRef.current.focus();
        }

        prevOpen.current = open;
    }, [open]);

    const onClickFilterOption = (event, filter) => {
        showFilter(filter);
        handleClose(event);
    }

    return <>
        <Button variant="text"
                ref={anchorRef}
                id="composition-button"
                aria-controls={open ? 'composition-menu' : undefined}
                aria-expanded={open ? 'true' : undefined}
                aria-haspopup="true"
                onClick={handleToggle}
                className={className}
        >
            <FilterIcon/>
            <span style={{marginLeft: '8px'}}>Добавить фильтр</span>
        </Button>
        <Popper
            open={open}
            anchorEl={anchorRef.current}
            role={undefined}
            placement="bottom-start"
            transition
            disablePortal
            style={{zIndex: '10'}}
        >
            {({TransitionProps, placement}) => (
                <Grow
                    {...TransitionProps}
                    style={{
                        transformOrigin:
                            placement === 'bottom-start' ? 'left top' : 'left bottom',
                    }}
                >
                    <Paper>
                        <ClickAwayListener onClickAway={handleClose}>
                            <MenuList
                                autoFocusItem={open}
                                id="composition-menu"
                                aria-labelledby="composition-button"
                                onKeyDown={handleListKeyDown}
                                sx={{
                                    backgroundColor: 'rgba(37,59,77,1)',
                                    color: 'rgba(146,157,166,1)'
                                }}
                            >
                                {
                                    filters.map(filter =>
                                        <MenuItem key={filter.props.source}
                                                  onClick={(event) => onClickFilterOption(event, filter.props.source)}
                                                  sx={{
                                                      '&&:hover': {
                                                          backgroundColor: 'rgba(26,41,54,1)',
                                                          color: 'rgba(245,246,247,1)'
                                                      }
                                                  }}>
                                            {filter.props.label}
                                        </MenuItem>)
                                }
                            </MenuList>
                        </ClickAwayListener>
                    </Paper>
                </Grow>
            )}
        </Popper>
    </>
}

export default FilterButton;