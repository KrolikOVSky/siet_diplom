import * as React from "react";
import {Link} from "react-router-dom";
import {Breadcrumbs} from "@material-ui/core";
import {map} from "lodash";
import messages from "../messages";
import {Chip} from "@mui/material";
import {ChevronRight as ChevronRightIcon, Home as HomeIcon} from "@mui/icons-material";


export const getDirectoriesFromPath = (directoryPath, entityType) => {
    const dirs = decodeURIComponent(directoryPath).split('/').filter(dir => dir !== '');
    const subPaths = [{
        name: messages.fileUploads.archive.label[entityType],
        path: ''
    }];
    for (let i = 0; i < dirs.length; i++) {
        subPaths.push({
            name: dirs[i],
            path: encodeURIComponent(dirs.slice(0, i + 1).join('/') + '/')
        });
    }
    return subPaths;
}

const DirectoryPathBreadcrumbs = ({directories, baseUrl}) => {
    const chipStyles = {
        '&:hover': {
            textDecoration: directories.length > 1 ? 'underline' : 'none',
            cursor: directories.length > 1 ? 'pointer' : 'default'
        }
    }
    return <Breadcrumbs aria-label="breadcrumb"
                        style={{marginLeft: '1em'}}
                        separator={<ChevronRightIcon/>}
    >
        {map(directories, (dir, i) => i === 0
            ? <Chip key={i}
                    label={dir.name}
                    sx={chipStyles}
                    component={Link}
                    icon={<HomeIcon/>}
                    to={`${baseUrl}/${dir.path}`}

            />
            : i === (directories.length - 1)
                ? <Chip key={i}
                        label={dir.name}
                        color='primary'
                />
                : <Chip
                    label={dir.name}
                    component={Link}
                    to={`${baseUrl}/${dir.path}`}
                    sx={chipStyles}
                    key={i}
                />)}
    </Breadcrumbs>
}

export default DirectoryPathBreadcrumbs;