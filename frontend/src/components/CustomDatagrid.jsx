import {keyBy} from "lodash";
import {Datagrid, ListContextProvider, Loading, useQuery} from "react-admin";
import {useEffect, useState} from "react";
import {ResearchToolkitPagination} from "./Pagination";
import {SearchInput} from "./SearchInput";
import {Box} from "@material-ui/core";

export const CustomDatagrid = ({resource, searchBy = null, ...props}) => {
    const [page, setPage] = useState(1);
    const [perPage, setPerPage] = useState(10);
    const [filter, setFilter] = useState({})
    const [searchValue, setSearchValue] = useState("")
    const {data, total, loading, error, ...restProps} = useQuery({
        type: 'getList',
        resource,
        payload: {
            pagination: {page, perPage},
            sort: {field: 'id', order: 'ASC'},
            filter,
            keepPreviousData: true
        }
    });

    useEffect(() => {
        setFilter({[searchBy]: searchValue})
    }, [searchValue])

    if (loading) {
        return <Loading/>
    }
    if (error) {
        return <p>Ошибка: {error}</p>
    }

    return (
        <Box>
            {searchBy && <SearchInput onChange={e => {
                setTimeout(() => {
                    setSearchValue(e.target.value)
                }, 1000)
            }}/>}
            <ListContextProvider value={
                {
                    data: keyBy(data, 'id'),
                    ids: data.map(({id}) => id),
                    total,
                    page,
                    perPage,
                    setPage,
                    setPerPage,
                    currentSort: {field: 'id', order: 'ASC'},
                    selectedIds: [],
                    filter
                }
            }
            >
                <Datagrid {...props}
                />
                <ResearchToolkitPagination/>
            </ListContextProvider>
        </Box>
    )
}