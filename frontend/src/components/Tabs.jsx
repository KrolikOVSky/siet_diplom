import {Box, Tab as TabMui, Tabs as TabsMui} from "@mui/material";
import React, {cloneElement, useEffect, useState} from "react";
import PropTypes from "prop-types";

const getTabs = (children) => Array.of(children)
    .flat(1)
    .filter(child => (typeof child === 'object') && child.props != null)
    .map((child, index) => cloneElement(child, {order: index}));

/**
 * Элемент для отображения и работы с вкладками
 *
 * @param children Обязательно чтобы элементы были {@link Tab Tab}
 * @param props
 * @returns "Компонент с вкладками"
 * @see Tab
 */

export const Tabs = ({children, ...props}) => {
    const [tab, setTab] = useState(0);
    const [allowedTabs, setAllowedTabs] = useState([]);

    useEffect(() => {
        setTab(0)
        setAllowedTabs(getTabs(children))
    }, [children])

    const handleChangeTab = (event, value) => setTab(value);

    return <Box {...props}>
        <TabsMui value={tab}
                 onChange={handleChangeTab}
                 variant={allowedTabs.length > 1 ? 'fullWidth' : 'standard'}
        >
            {allowedTabs.map(child => <TabMui label={child?.props?.title}/>)}
        </TabsMui>
        {allowedTabs.filter(child => child?.props.order === tab)}
    </Box>
}

/**
 * Элемент реализующий создание самой вкладки и ее содержания.
 * Используется в компоненте {@link Tabs Tabs}
 *
 * @param children Любое содержание
 * @param title Название вкладки
 * @see Tabs
 */

export const Tab = ({children}) => !!children && children
Tab.propTypes = {
    title: PropTypes.string.isRequired
}