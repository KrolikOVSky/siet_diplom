import * as React from "react";
import Button from '@material-ui/core/Button';
import {Add as AddIcon} from "@mui/icons-material";

const CreateButton = ({...restProps}) => <Button startIcon={<AddIcon/>}
                                                 {...restProps}/>

export default CreateButton;