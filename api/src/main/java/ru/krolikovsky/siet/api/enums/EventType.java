package ru.krolikovsky.siet.api.enums;

/**
 * Перечисление типов событий
 */
@SuppressWarnings("unused")
public enum EventType {
    /**
     * Пользователь успешно авторизован
     */
    USER_LOGGED_IN,
    /**
     * Пользователь ввел неправильный пароль
     */
    USER_FAILED_TO_LOG_IN,
    /**
     * пользователь сменил пароль
     */
    USER_CHANGED_PASSWORD,
    /**
     * Пользователь успешно создан
     */
    USER_CREATED,
    /**
     * Информация пользователя успешно обновлена
     */
    USER_UPDATED,
    /**
     * Пользователь успешно удален
     */
    USER_DELETED
}
