package ru.krolikovsky.siet.sietModule.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;
import ru.krolikovsky.siet.common.model.BaseSoftDeletedEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

import static ru.krolikovsky.siet.common.model.BaseSoftDeletedEntity.IS_DELETED_COLUMN_NAME;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "subjects")
@Data
@Where(clause = IS_DELETED_COLUMN_NAME + " = false")
public class Subject extends BaseSoftDeletedEntity {
    private String title;
    private String description;
}
