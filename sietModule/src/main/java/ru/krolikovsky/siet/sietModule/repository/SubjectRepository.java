package ru.krolikovsky.siet.sietModule.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.krolikovsky.siet.sietModule.model.Subject;

import java.util.UUID;

public interface SubjectRepository extends JpaRepository<Subject, UUID> {
    Page<Subject> findAllByTitleContainingIgnoreCase(String title, Pageable pageRequest);
}
