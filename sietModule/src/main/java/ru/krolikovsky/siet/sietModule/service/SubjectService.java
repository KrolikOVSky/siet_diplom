package ru.krolikovsky.siet.sietModule.service;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.krolikovsky.siet.common.authorization.ForAdmin;
import ru.krolikovsky.siet.common.dto.PageDto;
import ru.krolikovsky.siet.common.util.UuidConverter;
import ru.krolikovsky.siet.common.web.MarmelabUtils;
import ru.krolikovsky.siet.sietModule.model.Subject;
import ru.krolikovsky.siet.sietModule.model.dto.SubjectDto;
import ru.krolikovsky.siet.sietModule.model.mappers.SubjectMapper;
import ru.krolikovsky.siet.sietModule.repository.SubjectRepository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Objects;

@Service
@AllArgsConstructor
public class SubjectService {
    private SubjectRepository subjectRepository;
    private SubjectMapper subjectMapper;

    @Transactional
    public PageDto<SubjectDto> list(int _start, int _end, String _sort, String _order, String title) {
        Pageable pageRequest = MarmelabUtils.makePageRequest(_start, _end, _sort, _order);
        Page<Subject> subjects = title == null
                ? subjectRepository.findAll(pageRequest)
                : subjectRepository.findAllByTitleContainingIgnoreCase(title, pageRequest);
        return new PageDto<>(
                subjects.stream()
                        .map(subject -> subjectMapper.toSubjectDto(subject))
                        .toList(),
                subjects.getTotalElements());
    }


    @Transactional
    @ForAdmin
    public SubjectDto create(SubjectDto subjectDto) {
        Subject subject = subjectMapper.fromSubjectDto(subjectDto);
        subject.setCreateDate(LocalDateTime.now());
        subject.setLastUpdate(LocalDateTime.now());
        return subjectMapper.toSubjectDto(subjectRepository.saveAndFlush(subject));
    }

    @Transactional
    @ForAdmin
    public SubjectDto update(SubjectDto subjectDto, String id) {
        Subject subject = subjectRepository.findById(Objects.requireNonNull(UuidConverter.toUuid(id))).orElseThrow();
        subjectMapper.copyForUpdate(subjectDto, subject);

        subject = subjectRepository.saveAndFlush(subject);
        return subjectMapper.toSubjectDto(subject);
    }

    @Transactional
    @ForAdmin
    public SubjectDto delete(String id) {
        Subject subject = subjectRepository.getById(Objects.requireNonNull(UuidConverter.toUuid(id)));
        subject.setDeleted(true);
        subject = subjectRepository.saveAndFlush(subject);
        return subjectMapper.toSubjectDto(subject);
    }

    @Transactional
    public SubjectDto getOne(String id) {
        return subjectMapper.toSubjectDto(subjectRepository.getById(Objects.requireNonNull(UuidConverter.toUuid(id))));
    }
}
