package ru.krolikovsky.siet.sietModule.model.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.stereotype.Component;
import ru.krolikovsky.siet.common.mapper.BaseMapper;
import ru.krolikovsky.siet.sietModule.model.Subject;
import ru.krolikovsky.siet.sietModule.model.dto.SubjectDto;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;


@Component
@Mapper(componentModel = "spring", imports = {Date.class, LocalDateTime.class})
public abstract class SubjectMapper extends BaseMapper {

    public abstract Subject fromSubjectDto(SubjectDto subjectDto);

    public abstract SubjectDto toSubjectDto(Subject subject);

    public abstract List<SubjectDto> toSubjectDtos(List<Subject> subject);

    @Mapping(target = "lastUpdate",
            expression = "java(LocalDateTime.now())")
    public abstract void copyForUpdate(SubjectDto request, @MappingTarget Subject subject);
}
