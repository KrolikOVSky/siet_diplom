package ru.krolikovsky.siet.sietModule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.krolikovsky.siet.api.ApiApplication;
import ru.krolikovsky.siet.common.CommonApplication;

@SpringBootApplication(scanBasePackageClasses = {CommonApplication.class,
        SietModuleApplication.class, ApiApplication.class}

)
public class SietModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SietModuleApplication.class, args);
    }
}
