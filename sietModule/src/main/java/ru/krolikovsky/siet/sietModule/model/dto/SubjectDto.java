package ru.krolikovsky.siet.sietModule.model.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class SubjectDto {
    private UUID id;
    private String title;
    private String description;
    private LocalDateTime createDate;
    private LocalDateTime lastUpdate;
    private boolean isDeleted;
}
