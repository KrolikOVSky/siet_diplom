package ru.krolikovsky.siet.sietModule.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.CorsFilter;
import ru.krolikovsky.siet.common.auth.JwtTokenFilter;
import ru.krolikovsky.siet.common.auth.SecurityConfigUtils;

import javax.annotation.Resource;

/**
 * {@code SecurityConfig} - это класс-конфигурация для фильтрации http запросов
 *
 * @see <a href="https://www.toptal.com/spring/spring-security-tutorial">Spring Security Tutorial</a>
 */

@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Resource
    private JwtTokenFilter jwtTokenFilter;
    @Resource
    private SecurityConfigUtils securityConfigUtils;

    /**
     * Метод {@code configure} устанавливает настройки безопасности для Spring Security
     *
     * @param http тело http запроса
     * @throws Exception
     */

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Enable CORS and disable CSRF
        http = securityConfigUtils.configureSharedSetting(http);

        http.authorizeRequests()
                .anyRequest().authenticated();

        http.addFilterBefore(
                jwtTokenFilter,
                UsernamePasswordAuthenticationFilter.class
        );
    }

    /**
     * Метод {@code corsFilter} для фильтрации общего доступа к ресурсам для разных источников
     *
     * @return возвращает экземпляр класса {@link CorsFilter}
     */

    @Bean
    public CorsFilter corsFilter() {
        return securityConfigUtils.corsFilter();
    }

}
