package ru.krolikovsky.siet.sietModule.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import ru.krolikovsky.siet.common.dto.PageDto;
import ru.krolikovsky.siet.common.web.MarmelabUtils;
import ru.krolikovsky.siet.sietModule.model.dto.SubjectDto;
import ru.krolikovsky.siet.sietModule.service.SubjectService;

import java.util.Arrays;
import java.util.List;

import static ru.krolikovsky.siet.common.Constants.API_PATH_PREFIX_SIET_MODULE;

@RestController
@RequestMapping(path = API_PATH_PREFIX_SIET_MODULE + "/subjects")
@AllArgsConstructor
public class SubjectController {

    private SubjectService subjectService;

    @GetMapping(params = "!id")
    public ResponseEntity<List<SubjectDto>> list(@RequestParam(required = false) Integer _end,
                                                 @RequestParam(required = false) Integer _start,
                                                 @RequestParam(required = false) String _sort,
                                                 @RequestParam(required = false) String _order,
                                                 @RequestParam(required = false) String title) {
        PageDto<SubjectDto> subjectDtos = subjectService.list(_start, _end, _sort, _order, title);

        return ResponseEntity.ok()
                .header(MarmelabUtils.TABLE_TOTAL_COUNT_HEADER, Long.toString(subjectDtos.getTotal()))
                .body(subjectDtos.getDtos());
    }

    @GetMapping(params = "id")
    public ResponseEntity<List<SubjectDto>> getSubjectForLookupList(@RequestParam(value = "id") String id) {
        List<SubjectDto> subjectDtos = Arrays.stream(id.split(","))
                .filter(StringUtils::hasText)
                .map(subjectService::getOne)
                .toList();
        return ResponseEntity.ok()
                .body(subjectDtos);
    }

    @PostMapping
    public ResponseEntity<SubjectDto> create(@RequestBody SubjectDto subjectDto) {
        return ResponseEntity.ok()
                .body(subjectService.create(subjectDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<SubjectDto> edit(@RequestBody SubjectDto subjectDto, @PathVariable String id) {
        return ResponseEntity.ok()
                .body(subjectService.update(subjectDto, id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<SubjectDto> delete(@PathVariable String id) {
        return ResponseEntity.ok()
                .body(subjectService.delete(id));
    }

    @GetMapping("/{id}")
    public ResponseEntity<SubjectDto> getOne(@PathVariable String id) {
        return ResponseEntity.ok()
                .body(subjectService.getOne(id));
    }
}
