#!/bin/bash

PROJECT_DIR=/mnt/src/researcher-toolkit

complile_module(){
	MODULE_NAME=$1
	echo "Собираю модуль $MODULE_NAME"
	cp $MODULE_NAME/src/main/resources/application.yml.sample $MODULE_NAME/src/main/resources/application.yml
	./gradlew clean $MODULE_NAME:assemble
	mkdir $PROJECT_DIR/package/$MODULE_NAME/
	cp $MODULE_NAME/build/libs/*.jar $PROJECT_DIR/package/$MODULE_NAME/
	rm $MODULE_NAME/build/libs/*.jar
	cp $PROJECT_DIR/infrastructure/$MODULE_NAME/* $PROJECT_DIR/package/$MODULE_NAME
	echo "Готово"
}

compile_frontend(){
	echo "Собираю фронтенд инструмента исследователя"
	cd $PROJECT_DIR/frontend

	export NVM_DIR="$HOME/.nvm"
	[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
	[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

	nvm use v16.13.2
	npm install
	npm run build

	cp -r $PROJECT_DIR/frontend/build/* $PROJECT_DIR/package/frontend
	cp -r $PROJECT_DIR/frontend/public/tinymce $PROJECT_DIR/package/frontend
	cp -r $PROJECT_DIR/frontend/public/pdf-viewer $PROJECT_DIR/package/frontend
	cp -r $PROJECT_DIR/frontend/public/djvu-viewer $PROJECT_DIR/package/frontend
	cp -r $PROJECT_DIR/frontend/build/static $PROJECT_DIR/package/frontend
	cp -r $PROJECT_DIR/infrastructure/nginx/conf/* $PROJECT_DIR/package/frontend

}

BRANCH_NAME=$1
ENV_FILE=$2
if [ "$EUID" == 0 ]
  then echo "Не запускайте этот скрипт с правами root"
  exit
fi

if [ -z "$BRANCH_NAME" ]
then
    echo "Имя git ветки должно быть первым аргументом скрипта"
    exit 0
fi

cd $PROJECT_DIR

echo "Обновляюсь на ветку: $BRANCH_NAME"

git restore frontend/package-lock.json
git checkout $BRANCH_NAME
git pull origin $BRANCH_NAME

echo "Создаю каталог куда будут скопированы бинарники"
rm -rf ./package
mkdir -p package/frontend
mkdir -p package/postgres
mkdir -p package/nginx

compile_frontend

cd $PROJECT_DIR

complile_module webapp
complile_module fileupload

echo "Копирую дополнительные файлы"
cp $PROJECT_DIR/infrastructure/.env-prod $PROJECT_DIR/package/
cp $PROJECT_DIR/infrastructure/postgres/initdb.sql $PROJECT_DIR/package/postgres/
cp $PROJECT_DIR/infrastructure/package/02_run_assembly.sh $PROJECT_DIR/package/
cp -r $PROJECT_DIR/infrastructure/nginx/* $PROJECT_DIR/package/nginx

cd $PROJECT_DIR
tar -zcvf package-`date +%d\%m\%Y`.tar.gz package
#rm -r package

echo "Пакет готов"
ls -lha package-*.gz

cd $PROJECT_DIR/infrastructure/package
