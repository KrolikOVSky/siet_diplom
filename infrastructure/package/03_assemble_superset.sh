#!/bin/bash

SUPERSET_DIR=/mnt/src/infocenter/superset
PACKAGE_SUPERSET_DIST_DIR=/mnt/src/researcher-toolkit/package-superset
PROJECT_DIR=/mnt/src/researcher-toolkit

make_superset_package(){
    #качаем npm зависимости superset frontend
    cd $SUPERSET_DIR/superset-frontend
    echo "Качаю NPM зависимости Superset"
    npm install -g npm@7
    npm install -f --no-optional --global webpack webpack-cli
    npm install -f --no-optional
    echo "Собираю фронтенд Superset"
    npm run build

    echo "Копирую Superset"

    rsync -av --progress -r $SUPERSET_DIR/* $PACKAGE_SUPERSET_DIST_DIR --exclude node_modules --exclude .git
}

echo "Начинаю сборку пакета с Superset"

rm -rf $PACKAGE_SUPERSET_DIST_DIR
mkdir -p $PACKAGE_SUPERSET_DIST_DIR

make_superset_package

echo "Копирование Superset завершено"
#если копировать docker image в tar архив то почему-то приложение не разворачивается на пустом сервере
#с ошибкой unable to find user root: no matching entries in passwd file
#sudo $PROJECT_DIR/infrastructure/package/03_assemble_superset_download_img.sh

cp -a $PROJECT_DIR/infrastructure/package/04_run_superset.sh $PACKAGE_SUPERSET_DIST_DIR
rm $PROJECT_DIR/package-superset-*.gz
tar -zcvf $PROJECT_DIR/package-superset-`date +%d\%m\%Y`.tar.gz -C $PACKAGE_SUPERSET_DIST_DIR .

echo "Архив с Superset собран"
