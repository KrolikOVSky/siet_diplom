#!/bin/bash
echo "Starting Researcher application"
sudo docker-compose -p siet -f webapp/docker-compose.yml --env-file .env-prod up -d --force-recreate
echo "Application started"
echo "Copying nginx confgs and assets"
sudo cp nginx/conf/researcher /etc/nginx/sites-available/
sudo rm -r /usr/share/nginx/siet/*
sudo mkdir -p /usr/share/nginx/siet

sudo mkdir -p /usr/share/nginx/siet/frontend
sudo cp -r frontend/* /usr/share/nginx/siet/
sudo cp -r frontend/static /usr/share/nginx/siet/frontend
sudo cp -r frontend/djvu-viewer /usr/share/nginx/siet/frontend
sudo cp -r frontend/pdf-viewer /usr/share/nginx/siet/frontend
sudo cp -r frontend/tinymce /usr/share/nginx/siet/frontend
sudo ln -sf /etc/nginx/sites-available/researcher /etc/nginx/sites-enabled/researcher

sudo service nginx reload
echo "Done"
