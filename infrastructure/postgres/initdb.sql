ALTER SYSTEM SET max_connections = 200;
ALTER SYSTEM SET max_prepared_transactions = 200;
ALTER SYSTEM RESET shared_buffers;
CREATE EXTENSION tablefunc;
--Create DB
DROP DATABASE IF EXISTS siet;
CREATE DATABASE siet;
GRANT ALL PRIVILEGES ON DATABASE siet TO postgres;
\connect siet;
CREATE SCHEMA IF NOT EXISTS siet;
