#!/bin/bash
PROJECT_DIR=/mnt/src/fil
SCRIPT_DIR=$PROJECT_DIR/infrastructure/dev
echo "Script dir is: $SCRIPT_DIR"

BRANCH_NAME=$1

if [ -z "$BRANCH_NAME" ]
then
    BRANCH_NAME='develop'
    echo "Setting $BRANCH_NAME branch by default"
fi

$SCRIPT_DIR/down.sh
$SCRIPT_DIR/up.sh $BRANCH_NAME .env-dev
exit 0
