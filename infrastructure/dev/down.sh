PROJECT_DIR=/mnt/src/fil

INF_DIR="$PROJECT_DIR/infrastructure/"
sudo docker-compose -p fil-dev -f $INF_DIR/webapp/docker-compose.yml -f $INF_DIR/books/docker-compose.yml  --env-file $INF_DIR/.env-dev down
echo "fil stopped"
