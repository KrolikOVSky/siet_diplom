#!/bin/bash

PROJECT_DIR=/mnt/src/fil

complile_module(){
	MODULE_NAME=$1
	echo "Compiling $MODULE_NAME project"
	cp $MODULE_NAME/src/main/resources/application.yml.sample $MODULE_NAME/src/main/resources/application.yml
	./gradlew $MODULE_NAME:assemble
	cp $MODULE_NAME/build/libs/*.jar infrastructure/$MODULE_NAME/
	echo "done"
	echo "Removing old docker containers for $MODULE_NAME"
	sudo docker image rm fil-dev_$MODULE_NAME
}

BRANCH_NAME=$1
ENV_FILE=$2
if [ "$EUID" == 0 ]
  then echo "Please do not run this script as root"
  exit
fi

if [ -z "$BRANCH_NAME" ]
then
    echo "Branch name should be the first script argument"
    exit 0
fi

if [ -z "$ENV_FILE" ]
then
    echo "Env file not specified. Using default .env-dev"
    ENV_FILE=".env-dev"
fi


cd $PROJECT_DIR

echo "Updating to branch: $BRANCH_NAME"

git checkout $BRANCH_NAME
git pull origin $BRANCH_NAME

echo "Building frontend"
cd $PROJECT_DIR/frontend

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

npm ci
npm run build

sudo $PROJECT_DIR/infrastructure/dev/copy_nginx_assets.sh

cd $PROJECT_DIR

./gradlew clean
complile_module webapp
complile_module books

cd $PROJECT_DIR/infrastructure
echo "Starting docker containers"
sudo docker-compose -p fil-dev -f webapp/docker-compose.yml -f books/docker-compose.yml  --env-file $ENV_FILE up -d --build
rm webapp/*.jar
rm books/*.jar
echo "All done"
read -p "Press enter to finish"

