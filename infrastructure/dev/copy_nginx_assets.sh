echo "Uploading frontend assets to nginx"

PROJECT_DIR=/mnt/src/fil

rm -r /usr/share/nginx/fil/*
cp -r $PROJECT_DIR/frontend/build/* /usr/share/nginx/fil/
cp -r $PROJECT_DIR/infrastructure/nginx/conf/* /etc/nginx/sites-available/
ln -sf /etc/nginx/sites-available/fil.conf /etc/nginx/sites-enabled/
certbot -n --nginx -d litera21.ru --register-unsafely-without-email
systemctl restart nginx

echo "frontend assets uploaded"
