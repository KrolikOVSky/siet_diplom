#!/bin/bash
COMMAND_TYPE=$1
if [[ "" == "$COMMAND_TYPE" ]]; then
	echo "Additional command not specified. Doing default (infrastructure startup)"
	COMMAND_TYPE="up -d"
fi


if [  -d "infrastructure" ]
then
	$RUN_FROM_ROOT_DIR=1
	cd infrastructure/dev
else
	cd ../
fi

sudo docker-compose -p fil-inf -f postgres/docker-compose.yml --env-file .env-dev $COMMAND_TYPE
